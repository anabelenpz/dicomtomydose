select ca.numerohc, to_char(ca.fecha, '%Y-%m-%d')
from cex_actividad ca
	join cex_prestacion cp on ca.prest = cp.codigo
where year(fecha) = 2012
	and cp.codautonom = 'P43.5'	
	and ca.estado in ('CAP', 'PRG')
