package abpz.dicomtomydose.dcmqr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import abpz.dicomtomydose.utils.D4CUtils;
import abpz.dicomtomydose.utils.DateTimeUtils;
import abpz.dicomtomydose.utils.OutFormatUtils;

public class DcmQrCT extends DcmQr {
	private String[] models = { "HiSpeed NX/i", "LightSpeed RT",
			"LightSpeed Ultra", "Brilliance 40" };
	private String[] doseSummaryModels = { "Brilliance 40" };

	public DcmQrCT(String name) {
		super(name);
	}

	@Override
	protected String prepareModalityHeader(BasicDicomObject fmi, File file,
			Boolean active) {
		String ctHeader = "";
		if (active) {
			/* CT Images & Radiation Dose */
			ctHeader += "KVp;Data Collection Diameter(mm);Gantry/Detector Tilt;Table Height(mm);"
					+ "Rotation Direction;";

			/* For studies that has not dose summary */
			for (int i = 0; i < models.length; i++)
				if (fmi.getString(Tag.ManufacturerModelName).equals(models[i])) {
					ctHeader += "Scan Options;Series Number;Slice Thickness (mm);Slice Location (mm);"
							+ "Reconstruction Diameter(mm);Distance Source to Detector(mm);Distance Source to Patient(mm);"
							+ "Exposure Time(ms);Exposure(mAs);X-Ray Tube Current(mA);Focal Spot(s)(mm);";
					break;
				}

			/* Only for GE models */
			if (fmi.getString(Tag.Manufacturer).equals("GE MEDICAL SYSTEMS"))
				ctHeader += "Table Speed (mm/rotation);Beam Thickness;Scan Pitch Ratio (HS o HQ);Auto mA Mode;";

			/* Only for studies with dose summary */
			for (int i = 0; i < doseSummaryModels.length; i++)
				if (fmi.getString(Tag.ManufacturerModelName).equals(
						doseSummaryModels[i]))
					if (D4CUtils.hasDicomObjectSequence(fmi,
							Tag.ExposureDoseSequence))
						ctHeader += "Distance Source to Detector (mm);Total Number of Exposures;Total DLP(mGy*cm);";
		}

		/*
		 * The header for Exposure Dose Sequence stays into
		 * prepareModalityValues() for each Item that Exposure Dose Sequence has
		 */
		return ctHeader;

	}

	@Override
	protected String prepareModalityValues(BasicDicomObject fmi, File file) {
		String ctValues = "";
		// CT Modality
		int ctImageTags[] = { Tag.KVP, Tag.DataCollectionDiameter, /* mm */
		Tag.GantryDetectorTilt, /* degrees */
		Tag.TableHeight, /* mm */
		Tag.RotationDirection };

		int radiationDoseTags[] = { Tag.DistanceSourceToDetector, /* mm */
		Tag.TotalNumberOfExposures, Tag.toTag("00E11021"), /* DLP total mGy-cm */};

		int doseItemTags[] = { Tag.KVP, Tag.ExposureTime, /* ms */
		Tag.RadiationMode, Tag.FilterType, Tag.ScanLength, /* mm */
		Tag.FilterMaterial, Tag.XRayTubeCurrentInuA, /* uA */
		Tag.AcquisitionType, Tag.SingleCollimationWidth,/* mm */
		Tag.TotalCollimationWidth, /* mm */
		Tag.SpiralPitchFactor, Tag.EstimatedDoseSaving, /* % */
		Tag.CTDIvol, /* mGy */
		Tag.SeriesNumber, Tag.SliceLocation, /* mm */
		Tag.toTag("00E11021") }; /* mGy-cm */

		int ctValuesPerSlice[] = { Tag.ScanOptions, Tag.SeriesNumber,
				Tag.SliceThickness, Tag.SliceLocation, /* mm */
				Tag.ReconstructionDiameter, Tag.DistanceSourceToDetector,
				Tag.DistanceSourceToPatient, Tag.ExposureTime, Tag.Exposure,
				Tag.XRayTubeCurrent, Tag.FocalSpots };

		int doubleValueTags[] = { Tag.DataCollectionDiameter,
				Tag.GantryDetectorTilt, Tag.TableHeight,
				Tag.DistanceSourceToDetector, Tag.toTag("00E11021"),
				Tag.ExposureTime, Tag.ScanLength, Tag.XRayTubeCurrentInuA,
				Tag.SingleCollimationWidth, Tag.TotalCollimationWidth,
				Tag.SpiralPitchFactor, Tag.EstimatedDoseSaving, Tag.CTDIvol,
				Tag.SliceLocation, Tag.SliceThickness,
				Tag.ReconstructionDiameter, Tag.DistanceSourceToPatient,
				Tag.Exposure, Tag.XRayTubeCurrent, Tag.FocalSpots };

		int hiSpeedPrivateTags[] = { Tag.toTag("00191023"),/*
															 * Table Speed
															 * (mm/rotation)
															 */
		Tag.toTag("004B1001"), /* Beam Thickness */
		Tag.toTag("00431027"), /*
								 * Scan Pitch Ratio (refers to Table feed (mm))
								 * mode values= ['HS'|'HQ'] HS = Hi Speed (more
								 * advance); HQ= Hi Quality (superposition)
								 */
		Tag.toTag("0043106E") } /* Auto mA mode */;

		int doubleHiSpeedPrivateTags[] = { Tag.toTag("00191023"),/*
																 * Table Speed
																 * (mm/rotation)
																 */
		Tag.toTag("004B1001"), /* Beam Thickness */};

		/* CT Images & Radiation Dose */
		ctValues += OutFormatUtils.formatSpecificValues(ctImageTags,
				doubleValueTags, fmi);

		/* For studies that has not dose summary */
		for (int i = 0; i < models.length; i++)
			if (fmi.getString(Tag.ManufacturerModelName).equals(models[i])) {
				ctValues += OutFormatUtils.formatSpecificValues(
						ctValuesPerSlice, doubleValueTags, fmi);
				break;
			}

		/* Only for GE models */
		if (fmi.getString(Tag.Manufacturer).equals("GE MEDICAL SYSTEMS")) {
			ctValues += OutFormatUtils.formatSpecificValues(hiSpeedPrivateTags,
					doubleHiSpeedPrivateTags, fmi);
		}

		/* Only for studies with dose summary */
		for (int i = 0; i < doseSummaryModels.length; i++) {
			if (fmi.getString(Tag.ManufacturerModelName).equals(
					doseSummaryModels[i])) {
				if (D4CUtils.hasDicomObjectSequence(fmi,
						Tag.ExposureDoseSequence)) {

					ctValues += OutFormatUtils.formatSpecificValues(
							radiationDoseTags, doubleValueTags, fmi);

					/* Special format for Comments on radiation dose */
					if (fmi.getString(Tag.CommentsOnRadiationDose) != null) {
						String[] comments = fmi
								.getStrings(Tag.CommentsOnRadiationDose);
						if (comments != null) {
							ctValues += "\nComments on radiation dose\n";
							for (int k = 0; k < comments.length; k++)
								ctValues += comments[k] + ";";
						} else
							ctValues += " ;";
					}

					ArrayList<DicomObject> exposureDoseSeq;

					try {
						exposureDoseSeq = D4CUtils.getDicomObjectSequence(file,
								Tag.ExposureDoseSequence);

						if (exposureDoseSeq != null
								&& !exposureDoseSeq.isEmpty()) {
							for (int k = 0; k < exposureDoseSeq.size(); k++) {
								/* Item i header */
								ctValues += "\nItem " + (k + 1);
								ctValues += "\nAcquisition DateTime;KVp;Exposure Time (ms);Radiation Mode;"
										+ "Filter Type;Scan Length;Filter Material;X-Ray Tube Current (microAmperios);Acquisition Type;Single Collimation Width (mm);"
										+ "Total Collimation Width (mm);Spiral Pitch Factor;Estimated Dose Saving (%); CTDIvol (mGy);"
										+ "Series Number;Slice Location (mm); DLP (mGy*cm);\n";

								/* Special format for tag Acquisition DateTime */
								if (exposureDoseSeq.get(k).getString(
										Tag.AcquisitionDateTime) != null)
									ctValues += DateTimeUtils
											.formatDateTime(exposureDoseSeq
													.get(k)
													.getString(
															Tag.AcquisitionDateTime))
											+ ";";
								else
									ctValues += ";";

								ctValues += OutFormatUtils
										.formatSpecificValues(
												doseItemTags,
												doubleValueTags,
												(BasicDicomObject) exposureDoseSeq
														.get(k));
							}
						}
					} catch (IOException e) {
						System.err.println("Exposure Dose Sequence NOT FOUND");
						e.printStackTrace();
					}
				}
			}
		}

		return ctValues;
	}

	@Override
	protected Boolean fulfillModalityRequirements(BasicDicomObject fmi) {
		if (fmi.getString(Tag.ManufacturerModelName) != null)
			for (int i = 0; i < models.length; i++)
				if (fmi.getString(Tag.ManufacturerModelName).equals(models[i]))
					/*
					 * A study from doseSummaryModels that has not dose summary
					 * is a study without interest
					 */
					for (int j = 0; j < doseSummaryModels.length; j++) {
						if (fmi.getString(Tag.ManufacturerModelName).equals(
								doseSummaryModels[j])
								&& !D4CUtils.hasDicomObjectSequence(fmi,
										Tag.ExposureDoseSequence))
							return false;

						else {
							if (newQuery
									|| D4CUtils.hasDicomObjectSequence(fmi,
											Tag.ExposureDoseSequence)
									|| expDoseSeq) {
								activeHeaders = true;
								expDoseSeq = (D4CUtils.hasDicomObjectSequence(
										fmi, Tag.ExposureDoseSequence)) ? true
										: false;
							}
							imageAnalysis = true;
						}
					}

		return imageAnalysis;
	}
}