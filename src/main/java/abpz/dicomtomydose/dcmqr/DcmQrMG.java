package abpz.dicomtomydose.dcmqr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import abpz.dicomtomydose.utils.D4CUtils;
import abpz.dicomtomydose.utils.OutFormatUtils;

public class DcmQrMG extends DcmQr {
	private String[] models = { "Mammomat Novation DR", "DSM" };

	public DcmQrMG(String name) {
		super(name);
	}

	@Override
	protected Boolean fulfillModalityRequirements(BasicDicomObject fmi) {
		if (fmi.getString(Tag.ManufacturerModelName) != null)
			for (int i = 0; i < models.length; i++)
				if (fmi.getString(Tag.ManufacturerModelName).equals(models[i])) {
					if (!currentEquipment.equals(models[i]) || newQuery)
						activeHeaders = true;
					imageAnalysis = true;
				}
		return imageAnalysis;
	}

	@Override
	protected String prepareModalityValues(BasicDicomObject fmi, File file) {
		String mgValues = "";
		ArrayList<DicomObject> viewCodeSeq;
		ArrayList<DicomObject> partialViewCodeSeq;
		ArrayList<DicomObject> viewModifierCodeSeq;

		int mammographyImageTags[] = { Tag.PositionerPrimaryAngle,
				Tag.PositionerSecondaryAngle, Tag.BreastImplantPresent,
				Tag.PartialView, Tag.PartialViewDescription };

		int radiationDoseTags[] = { Tag.CompressionForce, Tag.KVP,
				Tag.XRayTubeCurrent, Tag.ExposureTime, Tag.Exposure,
				Tag.DistanceSourceToDetector, Tag.DistanceSourceToPatient,
				Tag.BodyPartThickness, Tag.EntranceDoseInmGy,
				Tag.DistanceSourceToEntrance, Tag.AnodeTargetMaterial,
				Tag.RectificationType };

		int filtrationTags[] = { Tag.FilterType, Tag.FilterMaterial,
				Tag.FilterThicknessMinimum, Tag.FilterThicknessMaximum };

		int generationTags[] = { Tag.ExposureControlMode, Tag.FocalSpots };
		int gridTags[] = { Tag.Grid };

		int doubleValueTags[] = { Tag.PositionerPrimaryAngle,
				Tag.PositionerSecondaryAngle, Tag.CompressionForce, Tag.KVP,
				Tag.XRayTubeCurrent, Tag.ExposureTime, Tag.Exposure,
				Tag.DistanceSourceToDetector, Tag.DistanceSourceToPatient,
				Tag.DistanceSourceToEntrance, Tag.BodyPartThickness,
				Tag.EntranceDoseInmGy, Tag.OrganDose };

		mgValues += OutFormatUtils.formatSpecificValues(mammographyImageTags,
				doubleValueTags, fmi);

		/* Extracting information about image projection */
		try {
			partialViewCodeSeq = D4CUtils.getDicomObjectSequence(file,
					Tag.PartialView);
			viewCodeSeq = D4CUtils.getDicomObjectSequence(file,
					Tag.ViewCodeSequence);
			viewModifierCodeSeq = D4CUtils.getDicomObjectSequence(file,
					Tag.ViewModifierCodeSequence);

			if (viewModifierCodeSeq == null) {
				if (partialViewCodeSeq != null && !partialViewCodeSeq.isEmpty())
					for (int i = 0; i < partialViewCodeSeq.size(); i++)
						if (partialViewCodeSeq.get(i)
								.getString(Tag.CodeMeaning) != null)
							mgValues += partialViewCodeSeq.get(i).getString(
									Tag.CodeMeaning)
									+ ";";
			} else
				mgValues += ";";

			/* Abbreviation of View Position */
			if (fmi.getString(Tag.ViewPosition) != null
					&& fmi.getString(Tag.ImageLaterality) != null)
				mgValues += fmi.getString(Tag.ImageLaterality)
						+ fmi.getString(Tag.ViewPosition) + ";";
			else
				mgValues += ";";

			/* Full description of View Position and View Modifier if it exists */
			if (viewCodeSeq != null && !viewCodeSeq.isEmpty()) {
				for (int i = 0; i < viewCodeSeq.size(); i++)
					if (viewCodeSeq.get(i).getString(Tag.CodeMeaning) != null)
						mgValues += viewCodeSeq.get(i).getString(
								Tag.CodeMeaning)
								+ ";";
			} else
				mgValues += ";";

			if (viewModifierCodeSeq != null && !viewModifierCodeSeq.isEmpty()) {
				for (int i = 0; i < viewModifierCodeSeq.size(); i++)
					if (viewModifierCodeSeq.get(i).getString(Tag.CodeMeaning) != null)
						mgValues += viewModifierCodeSeq.get(i).getString(
								Tag.CodeMeaning)
								+ ";";
			} else
				mgValues += ";";

		} catch (IOException e) {
			System.err
					.println("Not available information about image projection");
			e.printStackTrace();
		}

		mgValues += OutFormatUtils.formatSpecificValues(radiationDoseTags,
				doubleValueTags, fmi);

		/* Mammary Glandular Dose. dGy to mGy */
		if (fmi.getString(Tag.OrganDose) != null) {
			Double organDose = (fmi.getDouble(Tag.OrganDose));
			organDose = organDose * 100;
			mgValues += organDose.toString() + ";";
		}

		mgValues += OutFormatUtils.formatSpecificValues(filtrationTags,
				doubleValueTags, fmi)
				+ OutFormatUtils.formatSpecificValues(generationTags,
						doubleValueTags, fmi)
				+ OutFormatUtils.formatSpecificValues(gridTags,
						doubleValueTags, fmi);

		return mgValues;
	}

	@Override
	protected String prepareModalityHeader(BasicDicomObject fmi, File file,
			Boolean active) {
		String mgHeader = "";
		if (active)
			mgHeader += "Positioner Primary Angle; Positioner Secondary Angle;Breast Implant Present;"
					+ "Partial View;Partial View Description;Partial View 2nd Description;"
					+ "View Position;View Position Description;View Modifier Description;"
					+ "Compression Force (newtons);KVp;X-Ray Tube Current (mA);Exposure Time (ms);Exposure(mAs);"
					+ "Distance Source to Detector (mm);Distance Source to Patient (mm);Body Part Thickness (mm);"
					+ "Entrance Dose (mGy);Distance Source to Entrance (mm);Anode Target Material;Rectification Type;"
					+ "Organ Dose (mGy);Filter Type;Filter Material;Filter Thickness Minimum (mm);"
					+ "Filter Thickness Maximum (mm);Exposure Control Mode;Focal Spot(s) (mm);Grid;";

		return mgHeader;
	}

}