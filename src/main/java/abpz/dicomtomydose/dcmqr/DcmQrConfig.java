package abpz.dicomtomydose.dcmqr;

import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.dcm4che2.net.CommandUtils;

import abpz.dicomtomydose.DicomtomyDose;
import abpz.dicomtomydose.utils.DateTimeUtils;

public class DcmQrConfig {

	protected static final int KB = 1024;

	protected static String[] TLS1 = { "TLSv1" };

	protected static String[] SSL3 = { "SSLv3" };

	protected static String[] NO_TLS1 = { "SSLv3", "SSLv2Hello" };

	protected static String[] NO_SSL2 = { "TLSv1", "SSLv3" };

	protected static String[] NO_SSL3 = { "TLSv1", "SSLv2Hello" };

	protected static char[] SECRET = { 's', 'e', 'c', 'r', 'e', 't' };

	protected String keyStoreURL = "resource:tls/test_sys_1.p12";

	protected char[] keyStorePassword = SECRET;

	protected char[] keyPassword = null;

	protected String trustStoreURL = "resource:tls/mesa_certs.jks";

	protected char[] trustStorePassword = SECRET;

	private String name = "DicomtomyDose";

	int fileBufferSize = 256;

	private Boolean reuseAssoc = false;

	protected Boolean cfind = true;
	protected Boolean cget = false;

	private Boolean evalRetrieveAET = false;

	private Boolean inputData = false;
	private Boolean outputData = false;
	private Boolean qParams = false;

	private Boolean retall = false;
	private Boolean blocked = false;
	private Boolean vmf = false;

	private Boolean notExtNegociation = false;
	private Boolean relationQR = false;
	private Boolean dateTimeMatching = false;
	private Boolean fuzzySemanticPersonNameMatching = false;

	private Boolean rParams = false;

	private Boolean ivrleTC = true;

	private Boolean tls = false;
	private Boolean tlsNeedClientAuth = false;
	private Boolean packPDV = false;
	private Boolean tcpNoDelay = false;

	private String calledAET;
	private String remoteAE;
	private String remoteHost = "127.0.0.1";
	private String remotePort = "104";

	private String callingAET;
	private String localAE;
	private String localHost;
	private String localPort;

	private int connectTimeout;
	private int associationReaperPeriod;
	private int dimseRspTimeout;
	private int retrieveRspTimeout;
	private int acceptTimeout;
	private int releaseTimeout;
	private int socketCloseDelay;
	private int maxPDULengthReceive;
	private int maxPDULengthSend;
	private int sendBufferSize;
	private int receiveBufferSize;
	private int maxOpsInvoked;
	private int maxOpsPerformed;

	private int cancelAfter = Integer.MAX_VALUE;

	protected int priority = 0;

	private String cipherTls;

	private String storeDestPath = ".";
	private String[] storeTCs;
	private String modality;
	private String movedest = getCalledAET();

	private String qrLevel = null;
	private String[] tlsProtocol;
	private String[] cuids;
	private String[] matchingKeys;
	private String[] returnKeys;

	// Statistic file names
	private String successFileName;
	private String failedFileName;

	// Input & Output file names
	private String inFileName = null;
	private String outFileName = null;

	@SuppressWarnings("unchecked")
	public void fromCommadLine(CommandLine cl) {
		setName(cl.hasOption("device") ? cl.getOptionValue("device")
				: getName());

		if (cl.hasOption("cstore")) {
			setStoreTCs(cl.getOptionValues("cstore"));
			setModality(getStoreTCs()[0]);
			setSuccessFileName("SuccessfulStudies-" + getModality() + "_"
					+ DateTimeUtils.getCurrentDate() + ".csv");
			setFailedFileName("FailedStudies-" + getModality() + "_"
					+ DateTimeUtils.getCurrentDate() + ".csv");
		}

		final List<String> argList = cl.getArgList();
		remoteAE = argList.get(0);

		String[] calledAETAddress = split(remoteAE, '@');
		setCalledAET(calledAETAddress[0]);
		setReuseAssoc((cl.hasOption("reuseassoc") ? true : false));

		if (calledAETAddress[1] != null) {
			String[] hostPort = split(calledAETAddress[1], ':');
			setRemoteHost(hostPort[0]);
			setRemotePort(hostPort[1]);
		}

		if (cl.hasOption("L")) {
			String localAE = cl.getOptionValue("L");
			String[] localPort = split(localAE, ':');
			if (localPort[1] != null) {
				setLocalPort(localPort[1]);
			}
			String[] callingAETHost = split(localPort[0], '@');
			setCallingAET(callingAETHost[0]);
			if (callingAETHost[1] != null) {
				setLocalHost(callingAETHost[1]);
			}
		}

		if (cl.hasOption("connectTO"))
			setConnectTimeout(parseInt(cl.getOptionValue("connectTO"),
					"illegal argument of option -connectTO", 1,
					Integer.MAX_VALUE));

		if (cl.hasOption("reaper"))
			setAssociationReaperPeriod(parseInt(cl.getOptionValue("reaper"),
					"illegal argument of option -reaper", 1, Integer.MAX_VALUE));

		if (cl.hasOption("cfindrspTO"))
			setDimseRspTimeout(parseInt(cl.getOptionValue("cfindrspTO"),
					"illegal argument osendBufferSizef option -cfindrspTO", 1,
					Integer.MAX_VALUE));

		if (cl.hasOption("cmoverspTO"))
			setRetrieveRspTimeout(parseInt("120000"
			/* cl.getOptionValue("cmoverspTO") */,
					"illegal argument of option -cmoverspTO", 1,
					Integer.MAX_VALUE));

		if (cl.hasOption("cgetrspTO"))
			setRetrieveRspTimeout(parseInt(cl.getOptionValue("cgetrspTO"),
					"illegal argument of option -cgetrspTO", 1,
					Integer.MAX_VALUE));

		if (cl.hasOption("acceptTO"))
			setAcceptTimeout(parseInt(cl.getOptionValue("acceptTO"),
					"illegal argument of option -acceptTO", 1,
					Integer.MAX_VALUE));

		if (cl.hasOption("releaseTO"))
			setReleaseTimeout(parseInt(cl.getOptionValue("releaseTO"),
					"illegal argument of option -releaseTO", 1,
					Integer.MAX_VALUE));

		if (cl.hasOption("soclosedelay"))
			setSocketCloseDelay(parseInt(cl.getOptionValue("soclosedelay"),
					"illegal argument of option -soclosedelay", 1, 10000));

		if (cl.hasOption("rcvpdulen"))
			setMaxPDULengthReceive(parseInt(cl.getOptionValue("rcvpdulen"),
					"illegal argument of option -rcvpdulen", 1, 10000) * KB);

		if (cl.hasOption("sndpdulen"))
			setMaxPDULengthSend(parseInt(cl.getOptionValue("sndpdulen"),
					"illegal argument of option -sndpdulen", 1, 10000) * KB);
		if (cl.hasOption("sosndbuf"))
			setSendBufferSize(parseInt(cl.getOptionValue("sosndbuf"),
					"illegal argument of option -sosndbuf", 1, 10000) * KB);
		if (cl.hasOption("sorcvbuf"))
			setReceiveBufferSize(parseInt(cl.getOptionValue("sorcvbuf"),
					"illegal argument of option -sorcvbuf", 1, 10000) * KB);
		if (cl.hasOption("filebuf"))
			setFileBufferSize(parseInt(cl.getOptionValue("filebuf"),
					"illegal argument of option -filebuf", 1, 10000) * KB);

		setPackPDV(!cl.hasOption("pdv1"));

		setTcpNoDelay(!cl.hasOption("tcpdelay"));

		setMaxOpsInvoked(cl.hasOption("async") ? parseInt(
				cl.getOptionValue("async"),
				"illegal argument of option -async", 0, 0xffff) : 1);

		setMaxOpsPerformed(cl.hasOption("cstoreasync") ? parseInt(
				cl.getOptionValue("cstoreasync"),
				"illegal argument of option -cstoreasync", 0, 0xffff) : 0);

		if (cl.hasOption("C"))
			setCancelAfter(parseInt(cl.getOptionValue("C"),
					"illegal argument of option -C", 1, Integer.MAX_VALUE));

		setPriority((cl.hasOption("lowprior")) ? CommandUtils.LOW : 0);

		setPriority((cl.hasOption("highprior")) ? CommandUtils.HIGH : 0);

		setCfind(!cl.hasOption("nocfind"));

		setCget(cl.hasOption("cget"));

		if (cl.hasOption("cmove"))
			setMovedest(cl.getOptionValue("cmove"));

		setEvalRetrieveAET(cl.hasOption("evalRetrieveAET"));

		if (cl.hasOption("P"))
			setQrLevel("P");
		else if (cl.hasOption("S"))
			setQrLevel("S");
		else if (cl.hasOption("I"))
			setQrLevel("I");

		setNotExtNegociation(cl.hasOption("noextneg"));

		setRelationQR(cl.hasOption("rel"));

		setDateTimeMatching(cl.hasOption("datetime"));

		setFuzzySemanticPersonNameMatching(cl.hasOption("fuzzy"));

		if (getCfind())
			setCuids(cl.getOptionValues("cfind"));

		if (cl.hasOption("infile")) {
			setInFileName(cl.getOptionValue("infile"));
			setInputData(true);
		}

		if (cl.hasOption("q")) {
			matchingKeys = cl.getOptionValues("q");
			setqParams(true);
		}

		if (cl.hasOption("outfile")) {
			setOutFileName(cl.getOptionValue("outfile"));
			setOutputData(true);
		}

		if (cl.hasOption("r"))
			setReturnKeys(cl.getOptionValues("r"));

		setIvrleTC(cl.hasOption("ivrle"));
		setTls(cl.hasOption("tls"));

		if (cl.hasOption("tls")) {
			setCipherTls(cl.getOptionValue("tls"));

			if (cl.hasOption("tls1"))
				setTlsProtocol(TLS1);
			else if (cl.hasOption("ssl3"))
				setTlsProtocol(SSL3);
			else if (cl.hasOption("no_tls1"))
				setTlsProtocol(NO_TLS1);
			else if (cl.hasOption("no_ssl3"))
				setTlsProtocol(NO_SSL3);
			else if (cl.hasOption("no_ssl2"))
				setTlsProtocol(NO_SSL2);

			setTlsNeedClientAuth(!cl.hasOption("noclientauth"));

			setKeyStoreURL(cl.hasOption("keystore") ? cl
					.getOptionValue("keystore") : getKeyStoreURL());
			setKeyStorePassword((String) (cl.hasOption("keystorepw") ? cl
					.getOptionValue("keystorepw") : getKeyStorePassword()));
			setKeyPassword((String) (cl.hasOption("keypw") ? cl
					.getOptionValue("keypw") : getKeyPassword()));
			setTrustStoreURL((String) (cl.hasOption("truststore") ? cl
					.getOptionValue("truststore") : getTrustStoreURL()));
			setTrustStorePassword((String) (cl.hasOption("truststorepw") ? cl
					.getOptionValue("truststorepw") : getTrustStorePassword()));
		}
	}

	protected static String[] split(String s, char delim) {
		String[] s2 = { s, null };
		int pos = s.indexOf(delim);
		if (pos != -1) {
			s2[0] = s.substring(0, pos);
			s2[1] = s.substring(pos + 1);
		}
		return s2;
	}

	protected static int parseInt(String s, String errPrompt, int min, int max) {
		try {
			int i = Integer.parseInt(s);
			if (i >= min && i <= max)
				return i;
		} catch (NumberFormatException e) {
			// parameter is not a valid integer; fall through to exit
		}
		DicomtomyDose.exit(errPrompt);
		throw new RuntimeException();
	}

	public static String[] getTLS1() {
		return TLS1;
	}

	public static void setTLS1(String[] tLS1) {
		TLS1 = tLS1;
	}

	public static String[] getSSL3() {
		return SSL3;
	}

	public static void setSSL3(String[] sSL3) {
		SSL3 = sSL3;
	}

	public static String[] getNO_TLS1() {
		return NO_TLS1;
	}

	public static void setNO_TLS1(String[] nO_TLS1) {
		NO_TLS1 = nO_TLS1;
	}

	public static String[] getNO_SSL2() {
		return NO_SSL2;
	}

	public static void setNO_SSL2(String[] nO_SSL2) {
		NO_SSL2 = nO_SSL2;
	}

	public static String[] getNO_SSL3() {
		return NO_SSL3;
	}

	public static void setNO_SSL3(String[] nO_SSL3) {
		NO_SSL3 = nO_SSL3;
	}

	public static char[] getSECRET() {
		return SECRET;
	}

	public static void setSECRET(char[] sECRET) {
		SECRET = sECRET;
	}

	public String getKeyStoreURL() {
		return keyStoreURL;
	}

	public void setKeyStoreURL(String keyStoreURL) {
		this.keyStoreURL = keyStoreURL;
	}

	public char[] getKeyStorePassword() {
		return keyStorePassword;
	}

	public void setKeyStorePassword(String keyStorePassword) {
		this.keyStorePassword = keyStorePassword.toCharArray();
	}

	public char[] getKeyPassword() {
		return keyPassword;
	}

	public void setKeyPassword(String keyPassword) {
		this.keyPassword = keyPassword.toCharArray();
	}

	public String getTrustStoreURL() {
		return trustStoreURL;
	}

	public void setTrustStoreURL(String trustStoreURL) {
		this.trustStoreURL = trustStoreURL;
	}

	public char[] getTrustStorePassword() {
		return trustStorePassword;
	}

	public void setTrustStorePassword(String trustStorePassword) {
		this.trustStorePassword = trustStorePassword.toCharArray();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFileBufferSize() {
		return fileBufferSize;
	}

	public void setFileBufferSize(int fileBufferSize) {
		this.fileBufferSize = fileBufferSize;
	}

	public Boolean getReuseAssoc() {
		return reuseAssoc;
	}

	public void setReuseAssoc(Boolean reuseAssoc) {
		this.reuseAssoc = reuseAssoc;
	}

	public Boolean getCfind() {
		return cfind;
	}

	public void setCfind(Boolean cfind) {
		this.cfind = cfind;
	}

	public Boolean getCget() {
		return cget;
	}

	public void setCget(Boolean cget) {
		this.cget = cget;
	}

	public Boolean getEvalRetrieveAET() {
		return evalRetrieveAET;
	}

	public void setEvalRetrieveAET(Boolean evalRetrieveAET) {
		this.evalRetrieveAET = evalRetrieveAET;
	}

	public Boolean getInputData() {
		return inputData;
	}

	public void setInputData(Boolean inputData) {
		this.inputData = inputData;
	}

	public Boolean getOutputData() {
		return outputData;
	}

	public void setOutputData(Boolean outputData) {
		this.outputData = outputData;
	}

	public Boolean getqParams() {
		return qParams;
	}

	public void setqParams(Boolean qParams) {
		this.qParams = qParams;
	}

	public Boolean getRetall() {
		return retall;
	}

	public void setRetall(Boolean retall) {
		this.retall = retall;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public Boolean getVmf() {
		return vmf;
	}

	public void setVmf(Boolean vmf) {
		this.vmf = vmf;
	}

	public Boolean getNotExtNegociation() {
		return notExtNegociation;
	}

	public void setNotExtNegociation(Boolean notExtNegociation) {
		this.notExtNegociation = notExtNegociation;
	}

	public Boolean getRelationQR() {
		return relationQR;
	}

	public void setRelationQR(Boolean relationQR) {
		this.relationQR = relationQR;
	}

	public Boolean getDateTimeMatching() {
		return dateTimeMatching;
	}

	public void setDateTimeMatching(Boolean dateTimeMatching) {
		this.dateTimeMatching = dateTimeMatching;
	}

	public Boolean getFuzzySemanticPersonNameMatching() {
		return fuzzySemanticPersonNameMatching;
	}

	public void setFuzzySemanticPersonNameMatching(
			Boolean fuzzySemanticPersonNameMatching) {
		this.fuzzySemanticPersonNameMatching = fuzzySemanticPersonNameMatching;
	}

	public Boolean getrParams() {
		return rParams;
	}

	public void setrParams(Boolean rParams) {
		this.rParams = rParams;
	}

	public Boolean getIvrleTC() {
		return ivrleTC;
	}

	public void setIvrleTC(Boolean ivrleTC) {
		this.ivrleTC = ivrleTC;
	}

	public Boolean getTls() {
		return tls;
	}

	public void setTls(Boolean tls) {
		this.tls = tls;
	}

	public Boolean getTlsNeedClientAuth() {
		return tlsNeedClientAuth;
	}

	public void setTlsNeedClientAuth(Boolean tlsNeedClientAuth) {
		this.tlsNeedClientAuth = tlsNeedClientAuth;
	}

	public Boolean getPackPDV() {
		return packPDV;
	}

	public void setPackPDV(Boolean packPDV) {
		this.packPDV = packPDV;
	}

	public Boolean getTcpNoDelay() {
		return tcpNoDelay;
	}

	public void setTcpNoDelay(Boolean tcpNoDelay) {
		this.tcpNoDelay = tcpNoDelay;
	}

	public String getCalledAET() {
		return calledAET;
	}

	public void setCalledAET(String calledAET) {
		this.calledAET = calledAET;
	}

	public String getRemoteAE() {
		return remoteAE;
	}

	public void setRemoteAE(String remoteAE) {
		this.remoteAE = remoteAE;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public String getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(String remotePort) {
		this.remotePort = remotePort;
	}

	public String getCallingAET() {
		return callingAET;
	}

	public void setCallingAET(String callingAET) {
		this.callingAET = callingAET;
	}

	public String getLocalAE() {
		return localAE;
	}

	public void setLocalAE(String localAE) {
		this.localAE = localAE;
	}

	public String getLocalHost() {
		return localHost;
	}

	public void setLocalHost(String localHost) {
		this.localHost = localHost;
	}

	public String getLocalPort() {
		return localPort;
	}

	public void setLocalPort(String localPort) {
		this.localPort = localPort;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getAssociationReaperPeriod() {
		return associationReaperPeriod;
	}

	public void setAssociationReaperPeriod(int associationReaperPeriod) {
		this.associationReaperPeriod = associationReaperPeriod;
	}

	public int getDimseRspTimeout() {
		return dimseRspTimeout;
	}

	public void setDimseRspTimeout(int dimseRspTimeout) {
		this.dimseRspTimeout = dimseRspTimeout;
	}

	public int getRetrieveRspTimeout() {
		return retrieveRspTimeout;
	}

	public void setRetrieveRspTimeout(int retrieveRspTimeout) {
		this.retrieveRspTimeout = retrieveRspTimeout;
	}

	public int getAcceptTimeout() {
		return acceptTimeout;
	}

	public void setAcceptTimeout(int acceptTimeout) {
		this.acceptTimeout = acceptTimeout;
	}

	public int getReleaseTimeout() {
		return releaseTimeout;
	}

	public void setReleaseTimeout(int releaseTimeout) {
		this.releaseTimeout = releaseTimeout;
	}

	public int getSocketCloseDelay() {
		return socketCloseDelay;
	}

	public void setSocketCloseDelay(int socketCloseDelay) {
		this.socketCloseDelay = socketCloseDelay;
	}

	public int getMaxPDULengthReceive() {
		return maxPDULengthReceive;
	}

	public void setMaxPDULengthReceive(int maxPDULengthReceive) {
		this.maxPDULengthReceive = maxPDULengthReceive;
	}

	public int getMaxPDULengthSend() {
		return maxPDULengthSend;
	}

	public void setMaxPDULengthSend(int maxPDULengthSend) {
		this.maxPDULengthSend = maxPDULengthSend;
	}

	public int getSendBufferSize() {
		return sendBufferSize;
	}

	public void setSendBufferSize(int sendBufferSize) {
		this.sendBufferSize = sendBufferSize;
	}

	public int getReceiveBufferSize() {
		return receiveBufferSize;
	}

	public void setReceiveBufferSize(int receiveBufferSize) {
		this.receiveBufferSize = receiveBufferSize;
	}

	public int getMaxOpsInvoked() {
		return maxOpsInvoked;
	}

	public void setMaxOpsInvoked(int maxOpsInvoked) {
		this.maxOpsInvoked = maxOpsInvoked;
	}

	public int getMaxOpsPerformed() {
		return maxOpsPerformed;
	}

	public void setMaxOpsPerformed(int maxOpsPerformed) {
		this.maxOpsPerformed = maxOpsPerformed;
	}

	public int getCancelAfter() {
		return cancelAfter;
	}

	public void setCancelAfter(int cancelAfter) {
		this.cancelAfter = cancelAfter;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getCipherTls() {
		return cipherTls;
	}

	public void setCipherTls(String cipherTls) {
		this.cipherTls = cipherTls;
	}

	public String getStoreDestPath() {
		return storeDestPath;
	}

	public void setStoreDestPath(String storeDestPath) {
		this.storeDestPath = storeDestPath;
	}

	public String[] getStoreTCs() {
		return storeTCs;
	}

	public void setStoreTCs(String[] storeTCs) {
		this.storeTCs = storeTCs;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public String getMovedest() {
		return movedest;
	}

	public void setMovedest(String movedest) {
		this.movedest = movedest;
	}

	public String getQrLevel() {
		return qrLevel;
	}

	public void setQrLevel(String qrLevel) {
		this.qrLevel = qrLevel;
	}

	public String[] getTlsProtocol() {
		return tlsProtocol;
	}

	public void setTlsProtocol(String[] tlsProtocol) {
		this.tlsProtocol = tlsProtocol;
	}

	public String[] getCuids() {
		return cuids;
	}

	public void setCuids(String[] cuids) {
		this.cuids = cuids;
	}

	public String[] getMatchingKeys() {
		return matchingKeys;
	}

	public void setMatchingKeys(String[] matchingKeys) {
		this.matchingKeys = matchingKeys;
	}

	public String[] getReturnKeys() {
		return returnKeys;
	}

	public void setReturnKeys(String[] returnKeys) {
		this.returnKeys = returnKeys;
	}

	public String getSuccessFileName() {
		return successFileName;
	}

	public void setSuccessFileName(String successFileName) {
		this.successFileName = successFileName;
	}

	public String getFailedFileName() {
		return failedFileName;
	}

	public void setFailedFileName(String failedFileName) {
		this.failedFileName = failedFileName;
	}

	public String getInFileName() {
		return inFileName;
	}

	public void setInFileName(String inFileName) {
		this.inFileName = inFileName;
	}

	public String getOutFileName() {
		return outFileName;
	}

	public void setOutFileName(String outFileName) {
		this.outFileName = outFileName;
	}

	public static int getKb() {
		return KB;
	}

}
