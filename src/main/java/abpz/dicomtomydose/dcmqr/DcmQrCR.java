package abpz.dicomtomydose.dcmqr;

import java.io.File;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.Tag;

import abpz.dicomtomydose.utils.OutFormatUtils;

public class DcmQrCR extends DcmQr {
	private String[] models = { "digital DIAGNOST", "SIEMENS FD-X",
			"\"Thunder Platform\"" };

	public DcmQrCR(String name) {
		super(name);
	}

	@Override
	protected Boolean fulfillModalityRequirements(BasicDicomObject fmi) {
		if (fmi.getString(Tag.ManufacturerModelName) != null)
			for (int i = 0; i < models.length; i++)
				if (fmi.getString(Tag.ManufacturerModelName).equals(models[i])) {
					if (!currentEquipment.equals(models[i]) || newQuery)
						activeHeaders = true;
					imageAnalysis = true;
				}
		return imageAnalysis;
	}

	@Override
	protected String prepareModalityValues(BasicDicomObject fmi, File file) {
		String crValues = "";
		int crImageTags[] = { Tag.ViewPosition, Tag.KVP, Tag.XRayTubeCurrent,
				Tag.Exposure, Tag.ExposureTime,
				Tag.ImageAndFluoroscopyAreaDoseProduct, Tag.FilterType,
				Tag.CollimatorGridName, Tag.FocalSpots, Tag.Grid };

		int doubleValueTags[] = { Tag.KVP, Tag.XRayTubeCurrent, Tag.Exposure,
				Tag.ExposureTime, Tag.ImageAndFluoroscopyAreaDoseProduct };

		int diDiPrivateTags[] = { Tag.Sensitivity };
		int aristosPrivateTags[] = { Tag.RelativeXRayExposure };
		int definiumPrivateTags[] = { Tag.toTag("00111006") /* Image dose */,
				Tag.toTag("00111031") /* Detected Exposure Index */};

		crValues += OutFormatUtils.formatSpecificValues(crImageTags,
				doubleValueTags, fmi);

		/* Printing private EI values */
		if (fmi.getString(Tag.ManufacturerModelName).equals(models[0]))
			crValues += OutFormatUtils.formatSpecificValues(diDiPrivateTags,
					null, fmi);

		if (fmi.getString(Tag.ManufacturerModelName).equals(models[1]))
			crValues += OutFormatUtils.formatSpecificValues(aristosPrivateTags,
					null, fmi);

		if (fmi.getString(Tag.ManufacturerModelName).equals(models[2]))
			crValues += OutFormatUtils.formatSpecificValues(
					definiumPrivateTags, null, fmi);

		return crValues;
	}

	@Override
	protected String prepareModalityHeader(BasicDicomObject fmi, File file,
			Boolean active) {
		String crHeader = "";
		if (active) {
			crHeader += "View Position;KVP;X-Ray Tube Current (mA);Exposure(mAs);Exposure Time (ms);"
					+ "Image and fluoroscopy DPA (dGy*cm²);Filter Type;Collimator/Grid Name;Focal Spot(s) (mm);Grid;";

			/*
			 * Printing private EI headers: Exposure Index(validate by AAPM
			 * 116);
			 */
			if (fmi.getString(Tag.ManufacturerModelName).equals(models[0]))
				crHeader += "Exposure Index (Sensibility)";

			if (fmi.getString(Tag.ManufacturerModelName).equals(models[1]))
				crHeader += "Exposure Index (Relative X-Ray Exposure)";

			if (fmi.getString(Tag.ManufacturerModelName).equals(models[2]))
				crHeader += "Exposure Index (Image dose); Exposure Index (Detected Exposure Index)";
		}
		return crHeader;

	}

}