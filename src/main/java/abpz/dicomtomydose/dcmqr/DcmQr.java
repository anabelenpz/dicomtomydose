package abpz.dicomtomydose.dcmqr;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.Executor;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.UIDDictionary;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.net.Association;
import org.dcm4che2.net.CommandUtils;
import org.dcm4che2.net.ConfigurationException;
import org.dcm4che2.net.Device;
import org.dcm4che2.net.DicomServiceException;
import org.dcm4che2.net.DimseRSP;
import org.dcm4che2.net.DimseRSPHandler;
import org.dcm4che2.net.ExtQueryTransferCapability;
import org.dcm4che2.net.ExtRetrieveTransferCapability;
import org.dcm4che2.net.NetworkApplicationEntity;
import org.dcm4che2.net.NetworkConnection;
import org.dcm4che2.net.NewThreadExecutor;
import org.dcm4che2.net.NoPresentationContextException;
import org.dcm4che2.net.PDVInputStream;
import org.dcm4che2.net.Status;
import org.dcm4che2.net.TransferCapability;
import org.dcm4che2.net.UserIdentity;
import org.dcm4che2.net.service.DicomService;
import org.dcm4che2.net.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import abpz.dicomtomydose.DicomtomyDose;
import abpz.dicomtomydose.utils.D4CUtils;
import abpz.dicomtomydose.utils.DateTimeUtils;
import abpz.dicomtomydose.utils.OutFormatUtils;
import abpz.dicomtomydose.utils.QueryStatistics;
import abpz.dicomtomydose.utils.QueryStatus;
/**
 * @author gunter zeilinger(gunterze@gmail.com)
 * @version $Revision: 13193 $ $Date: 2010-04-16 15:09:09 +0200 (Fri, 16 Apr
 *          2010) $
 * @since Jan, 2006
 * 
 * @author Ana Belén Peinado Zamora (anabelenpz@gmail.com)
 * @since Dec, 2012
 * 
 * Modify to export information of interest from dicom headers to a .csv file 
 * and also get 2 statistic files with successful and failed studies
 */

public abstract class DcmQr {
	//Configuration for DcmQr
	private DcmQrConfig qrConfig;
	
	//Parameters for Files
	private FileWriter ofile = null;
	private static BufferedWriter obw;
	
	//Parameters to file format
	protected static Boolean newQuery = true;
	protected Boolean activeHeaders = true;
	protected Boolean imageAnalysis = false;
	protected Boolean expDoseSeq = false;
	protected String currentEquipment = "";
		
	//Matching keys from input file
	protected ArrayList <String> patientIDs = new ArrayList<String>();
	protected ArrayList <String> studyDates = new ArrayList<String>();
	
	//Configuration to re-search of queries
	protected static Integer repeat = 0;
	protected static Boolean closeAssoc;
	protected static final long interval = 300000; /*5 minutes*/
	protected static Integer counter = 12; /* 12*10000ms (2 minutes) for timer */
	
	//Parameters for prepare studies to be moved and stored
	private Map<String, Integer> qProcessed = new LinkedHashMap<String, Integer>();
	private static ArrayList<QueryStatus> qPendingToStore = new ArrayList<QueryStatus>();
	private Integer currentCMoveRspID = -1;
	
	private Boolean abortSTorage = false;
	
	protected final Logger LOG = LoggerFactory
			.getLogger(this.getClass());

	public static enum QueryRetrieveLevel {
		PATIENT("PATIENT", PATIENT_RETURN_KEYS, PATIENT_LEVEL_FIND_CUID,
				PATIENT_LEVEL_GET_CUID, PATIENT_LEVEL_MOVE_CUID), STUDY(
				"STUDY", STUDY_RETURN_KEYS, STUDY_LEVEL_FIND_CUID,
				STUDY_LEVEL_GET_CUID, STUDY_LEVEL_MOVE_CUID), SERIES("SERIES",
				SERIES_RETURN_KEYS, SERIES_LEVEL_FIND_CUID,
				SERIES_LEVEL_GET_CUID, SERIES_LEVEL_MOVE_CUID), IMAGE("IMAGE",
				INSTANCE_RETURN_KEYS, SERIES_LEVEL_FIND_CUID,
				SERIES_LEVEL_GET_CUID, SERIES_LEVEL_MOVE_CUID);

		protected final String code;
		protected final int[] returnKeys;
		protected final String[] findClassUids;
		protected final String[] getClassUids;
		protected final String[] moveClassUids;

		QueryRetrieveLevel(String code, int[] returnKeys,
				String[] findClassUids, String[] getClassUids,
				String[] moveClassUids) {
			this.code = code;
			this.returnKeys = returnKeys;
			this.findClassUids = findClassUids;
			this.getClassUids = getClassUids;
			this.moveClassUids = moveClassUids;
		}

		public String getCode() {
			return code;
		}

		public int[] getReturnKeys() {
			return returnKeys;
		}

		public String[] getFindClassUids() {
			return findClassUids;
		}

		public String[] getGetClassUids() {
			return getClassUids;
		}

		public String[] getMoveClassUids() {
			return moveClassUids;
		}
	}

	protected static final String[] PATIENT_LEVEL_FIND_CUID = {
			UID.PatientRootQueryRetrieveInformationModelFIND,
			UID.PatientStudyOnlyQueryRetrieveInformationModelFINDRetired };

	protected static final String[] STUDY_LEVEL_FIND_CUID = {
			UID.StudyRootQueryRetrieveInformationModelFIND,
			UID.PatientRootQueryRetrieveInformationModelFIND,
			UID.PatientStudyOnlyQueryRetrieveInformationModelFINDRetired };

	protected static final String[] SERIES_LEVEL_FIND_CUID = {
			UID.StudyRootQueryRetrieveInformationModelFIND,
			UID.PatientRootQueryRetrieveInformationModelFIND, };

	protected static final String[] PATIENT_LEVEL_GET_CUID = {
			UID.PatientRootQueryRetrieveInformationModelGET,
			UID.PatientStudyOnlyQueryRetrieveInformationModelGETRetired };

	protected static final String[] STUDY_LEVEL_GET_CUID = {
			UID.StudyRootQueryRetrieveInformationModelGET,
			UID.PatientRootQueryRetrieveInformationModelGET,
			UID.PatientStudyOnlyQueryRetrieveInformationModelGETRetired };

	protected static final String[] SERIES_LEVEL_GET_CUID = {
			UID.StudyRootQueryRetrieveInformationModelGET,
			UID.PatientRootQueryRetrieveInformationModelGET };

	protected static final String[] PATIENT_LEVEL_MOVE_CUID = {
			UID.PatientRootQueryRetrieveInformationModelMOVE,
			UID.PatientStudyOnlyQueryRetrieveInformationModelMOVERetired };

	protected static final String[] STUDY_LEVEL_MOVE_CUID = {
			UID.StudyRootQueryRetrieveInformationModelMOVE,
			UID.PatientRootQueryRetrieveInformationModelMOVE,
			UID.PatientStudyOnlyQueryRetrieveInformationModelMOVERetired };

	protected static final String[] SERIES_LEVEL_MOVE_CUID = {
			UID.StudyRootQueryRetrieveInformationModelMOVE,
			UID.PatientRootQueryRetrieveInformationModelMOVE };

	protected static final int[] PATIENT_RETURN_KEYS = { Tag.PatientName,
			Tag.PatientID, Tag.PatientBirthDate, Tag.PatientSex,
			Tag.NumberOfPatientRelatedStudies,
			Tag.NumberOfPatientRelatedSeries,
			Tag.NumberOfPatientRelatedInstances };

	protected static final int[] PATIENT_MATCHING_KEYS = { Tag.PatientName,
			Tag.PatientID, Tag.IssuerOfPatientID, Tag.PatientBirthDate,
			Tag.PatientSex };

	protected static final int[] STUDY_RETURN_KEYS = { Tag.StudyDate,
			Tag.StudyTime, Tag.AccessionNumber, Tag.StudyID,
			Tag.StudyInstanceUID, Tag.NumberOfStudyRelatedSeries,
			Tag.NumberOfStudyRelatedInstances };

	protected static final int[] STUDY_MATCHING_KEYS = { Tag.StudyDate,
			Tag.StudyTime, Tag.AccessionNumber, Tag.ModalitiesInStudy,
			Tag.ReferringPhysicianName, Tag.StudyID, Tag.StudyInstanceUID };

	protected static final int[] PATIENT_STUDY_MATCHING_KEYS = { Tag.StudyDate,
			Tag.StudyTime, Tag.AccessionNumber, Tag.ModalitiesInStudy,
			Tag.ReferringPhysicianName, Tag.PatientName, Tag.PatientID,
			Tag.IssuerOfPatientID, Tag.PatientBirthDate, Tag.PatientSex,
			Tag.StudyID, Tag.StudyInstanceUID };

	protected static final int[] SERIES_RETURN_KEYS = { Tag.Modality,
			Tag.SeriesNumber, Tag.SeriesInstanceUID,
			Tag.NumberOfSeriesRelatedInstances };

	protected static final int[] SERIES_MATCHING_KEYS = { Tag.Modality,
			Tag.SeriesNumber, Tag.SeriesInstanceUID,
			Tag.RequestAttributesSequence };

	protected static final int[] INSTANCE_RETURN_KEYS = { Tag.InstanceNumber,
			Tag.SOPClassUID, Tag.SOPInstanceUID, };

	protected static final int[] MOVE_KEYS = { Tag.QueryRetrieveLevel,
			Tag.PatientID, Tag.StudyInstanceUID, Tag.SeriesInstanceUID,
			Tag.SOPInstanceUID, };

	protected static final String[] IVRLE_TS = { UID.ImplicitVRLittleEndian };

	protected static final String[] NATIVE_LE_TS = { UID.ExplicitVRLittleEndian,
			UID.ImplicitVRLittleEndian };

	protected static final String[] NATIVE_BE_TS = { UID.ExplicitVRBigEndian,
			UID.ImplicitVRLittleEndian };

	protected static final String[] DEFLATED_TS = {
			UID.DeflatedExplicitVRLittleEndian, UID.ExplicitVRLittleEndian,
			UID.ImplicitVRLittleEndian };

	protected static final String[] NOPX_TS = { UID.NoPixelData,
			UID.ExplicitVRLittleEndian, UID.ImplicitVRLittleEndian };

	protected static final String[] NOPXDEFL_TS = { UID.NoPixelDataDeflate,
			UID.NoPixelData, UID.ExplicitVRLittleEndian,
			UID.ImplicitVRLittleEndian };

	protected static final String[] JPLL_TS = { UID.JPEGLossless,
			UID.JPEGLosslessNonHierarchical14, UID.JPEGLSLossless,
			UID.JPEG2000LosslessOnly, UID.ExplicitVRLittleEndian,
			UID.ImplicitVRLittleEndian };

	protected static final String[] JPLY_TS = { UID.JPEGBaseline1,
			UID.JPEGExtended24, UID.JPEGLSLossyNearLossless, UID.JPEG2000,
			UID.ExplicitVRLittleEndian, UID.ImplicitVRLittleEndian };

	protected static final String[] MPEG2_TS = { UID.MPEG2 };

	protected static final String[] DEF_TS = { UID.JPEGLossless,
			UID.JPEGLosslessNonHierarchical14, UID.JPEGLSLossless,
			UID.JPEGLSLossyNearLossless, UID.JPEG2000LosslessOnly,
			UID.JPEG2000, UID.JPEGBaseline1, UID.JPEGExtended24, UID.MPEG2,
			UID.DeflatedExplicitVRLittleEndian, UID.ExplicitVRBigEndian,
			UID.ExplicitVRLittleEndian, UID.ImplicitVRLittleEndian };

	protected static enum TS {
		IVLE(IVRLE_TS), LE(NATIVE_LE_TS), BE(NATIVE_BE_TS), DEFL(DEFLATED_TS), JPLL(
				JPLL_TS), JPLY(JPLY_TS), MPEG2(MPEG2_TS), NOPX(NOPX_TS), NOPXD(
				NOPXDEFL_TS);

		final String[] uids;

		TS(String[] uids) {
			this.uids = uids;
		}
	}

	protected static enum CUID {
		CR(UID.ComputedRadiographyImageStorage), CT(UID.CTImageStorage), MR(
				UID.MRImageStorage), US(UID.UltrasoundImageStorage), NM(
				UID.NuclearMedicineImageStorage), PET(
				UID.PositronEmissionTomographyImageStorage), SC(
				UID.SecondaryCaptureImageStorage), XA(
				UID.XRayAngiographicImageStorage), XRF(
				UID.XRayRadiofluoroscopicImageStorage), DX(
				UID.DigitalXRayImageStorageForPresentation), MG(
				UID.DigitalMammographyXRayImageStorageForPresentation), PR(
				UID.GrayscaleSoftcopyPresentationStateStorageSOPClass), KO(
				UID.KeyObjectSelectionDocumentStorage), SR(
				UID.BasicTextSRStorage);

		final String uid;

		CUID(String uid) {
			this.uid = uid;
		}

	}

	protected static final String[] EMPTY_STRING = {};

	protected final Executor executor;

	protected final static NetworkApplicationEntity remoteAE = new NetworkApplicationEntity();

	protected final NetworkConnection remoteConn = new NetworkConnection();

	protected final Device device;

	protected final NetworkApplicationEntity ae = new NetworkApplicationEntity();

	protected final NetworkConnection conn = new NetworkConnection();

	protected Association assoc;

	File storeDest;

	protected boolean devnull;

	protected QueryRetrieveLevel qrlevel = QueryRetrieveLevel.STUDY;

	protected List<String> privateFind = new ArrayList<String>();

	protected final List<TransferCapability> storeTransferCapability = new ArrayList<TransferCapability>(
			8);

	protected DicomObject keys = new BasicDicomObject();

	protected int completed;

	protected int warning;

	protected int failed;

	protected FileWriter writer = null;

	public DcmQr(String name) {
		device = new Device(name);
		executor = new NewThreadExecutor(name);
		remoteAE.setInstalled(true);
		remoteAE.setAssociationAcceptor(true);
		remoteAE.setNetworkConnection(new NetworkConnection[] { remoteConn });

		device.setNetworkApplicationEntity(ae);
		device.setNetworkConnection(conn);
		ae.setNetworkConnection(conn);
		ae.setAssociationInitiator(true);
		ae.setAssociationAcceptor(true);
		ae.setAETitle(name);
	}

	public final void setLocalHost(String hostname) {
		conn.setHostname(hostname);
	}

	public final void setLocalPort(int port) {
		conn.setPort(port);
	}

	public final void setRemoteHost(String hostname) {
		remoteConn.setHostname(hostname);
	}

	public final void setRemotePort(int port) {
		remoteConn.setPort(port);
	}

	public final void setTlsProtocol(String[] tlsProtocol) {
		conn.setTlsProtocol(tlsProtocol);
	}

	public final void setTlsWithoutEncyrption() {
		conn.setTlsWithoutEncyrption();
		remoteConn.setTlsWithoutEncyrption();
	}

	public final void setTls3DES_EDE_CBC() {
		conn.setTls3DES_EDE_CBC();
		remoteConn.setTls3DES_EDE_CBC();
	}

	public final void setTlsAES_128_CBC() {
		conn.setTlsAES_128_CBC();
		remoteConn.setTlsAES_128_CBC();
	}

	public final void setTlsNeedClientAuth(boolean needClientAuth) {
		conn.setTlsNeedClientAuth(needClientAuth);
	}

	public final void setCalledAET(String called, boolean reuse) {
		remoteAE.setAETitle(called);
		if (reuse)
			ae.setReuseAssocationToAETitle(new String[] { called });
	}

	public final void setCalling(String calling) {
		ae.setAETitle(calling);
	}

	public final void setUserIdentity(UserIdentity userIdentity) {
		ae.setUserIdentity(userIdentity);
	}

	public final void setConnectTimeout(int connectTimeout) {
		conn.setConnectTimeout(connectTimeout);
	}

	public final void setMaxPDULengthReceive(int maxPDULength) {
		ae.setMaxPDULengthReceive(maxPDULength);
	}

	public final void setMaxOpsInvoked(int maxOpsInvoked) {
		ae.setMaxOpsInvoked(maxOpsInvoked);
	}

	public final void setMaxOpsPerformed(int maxOps) {
		ae.setMaxOpsPerformed(maxOps);
	}

	public final void setPackPDV(boolean packPDV) {
		ae.setPackPDV(packPDV);
	}

	public final void setAssociationReaperPeriod(int period) {
		device.setAssociationReaperPeriod(period);
	}

	public final void setDimseRspTimeout(int timeout) {
		ae.setDimseRspTimeout(timeout);
	}

	public final void setRetrieveRspTimeout(int timeout) {
		ae.setRetrieveRspTimeout(timeout);
	}

	public final void setTcpNoDelay(boolean tcpNoDelay) {
		conn.setTcpNoDelay(tcpNoDelay);
	}

	public final void setAcceptTimeout(int timeout) {
		conn.setAcceptTimeout(timeout);
	}

	public final void setReleaseTimeout(int timeout) {
		conn.setReleaseTimeout(timeout);
	}

	public final void setSocketCloseDelay(int timeout) {
		conn.setSocketCloseDelay(timeout);
	}

	public final void setMaxPDULengthSend(int maxPDULength) {
		ae.setMaxPDULengthSend(maxPDULength);
	}

	public final void setReceiveBufferSize(int bufferSize) {
		conn.setReceiveBufferSize(bufferSize);
	}

	public final void setSendBufferSize(int bufferSize) {
		conn.setSendBufferSize(bufferSize);
	}
		
	public void addStoreTransferCapability(String cuid, String[] tsuids) {
		storeTransferCapability.add(new TransferCapability(cuid, tsuids,
				TransferCapability.SCP));
	}

	public boolean isEvalRetrieveAET() {
		return qrConfig.getEvalRetrieveAET();
	}
	
	public final int getFailed() {
		return failed;
	}

	public final int getWarning() {
		return warning;
	}

	public final int getTotalRetrieved() {
		return completed + warning;
	}

	public void addMatchingKey(int[] tagPath, String value) {
		keys.putString(tagPath, null, value);
	}

	public void addReturnKey(int[] tagPath) {
		keys.putNull(tagPath, null);
	}

	public void dicomtomyDose(){
		/* Establishing TCP connection*/
		try {
			start();
		} catch (Exception e) {
			LOG.error("ERROR: Failed to start server for receiving "
					+ "requested objects:" + e.getMessage());
			System.exit(2);
		}
		
		//AddMatching keys and start run DcmAnalisis
		ArrayList <String> patients = getPatientIDs();
		ArrayList <String> dates = getStudyDates();
		Integer nqueries;
		
		if(qrConfig.getInputData() || qrConfig.getqParams()){
			nqueries = (qrConfig.getInputData() ? patients.size():1);
			for (int i = 0; i < nqueries; ++i){	
				for(;;){
					if(qrConfig.getInputData()){
						addMatchingKey(Tag.toTagPath("00100020"), patients.get(i));
						addMatchingKey(Tag.toTagPath("00080020"), dates.get(i));
					}
					
					try {
						long t1 = System.currentTimeMillis();
						try {
							/*Writing association request into the TCP socket */
							open();
						} catch (Exception e) {
							LOG.error("Failed to establish association: {}", e.toString());
							LOG.error("Failed query {} with Patient ID {} and Study Date {}",
									new Object [] {Integer.valueOf(i+1),
										keys.getString(Tag.PatientID),
										DateTimeUtils.formatDate(keys.getString(Tag.StudyDate))});
							if(counter > 0){
								Thread.sleep(10000);/* Wait 10s for reconnection*/
								LOG.info("Trying reconnection. Wait 10 seconds.You have "+counter+" attemps.");
								--i; /*Try with same matching keys again*/
								--counter;
							}else{
								LOG.error("Trying with next matching key if exists");
								qPendingToStore.add(new QueryStatus(null,
										keys.getString(Tag.PatientID), 
										keys.getString(Tag.StudyDate),
										null, 0, 0,
								"Error: Failed to establish association: "+e.toString()));
								getStatistics(QueryStatistics.failed, 0, 0, qrConfig.getFailedFileName());
							}
							break;
						}
						//Restart counter for next possible failed association
						counter = 12;
					
						String stringRemoteAET = remoteAE.getAETitle()+"@"+remoteConn.getHostname()+":"+remoteConn.getPort();
						
						long t2 = System.currentTimeMillis();
						LOG.info("Connected to {} in {} s", stringRemoteAET,
								Float.valueOf((t2 - t1) / 1000f));
		
						for (;;) {
							List<DicomObject> result;
							if (isCFind()) {
								result = query();
								long t3 = System.currentTimeMillis();
								LOG.info("Received {} matching entries in {} s",
										Integer.valueOf(result.size()),
										Float.valueOf((t3 - t2) / 1000f));
								t2 = t3;
								if(result.size() == 0){
									if(!keys.getString(Tag.PatientID).isEmpty() || !keys.getString(Tag.StudyDate).isEmpty()){
										LOG.error("Not found query {} with Patient ID {} and Study Date {}",
												new Object [] {Integer.valueOf(i+1),
												keys.getString(Tag.PatientID), 
												DateTimeUtils.formatDate(
													keys.getString(Tag.StudyDate))});
										qPendingToStore.add(new QueryStatus(null,
												keys.getString(Tag.PatientID), 
												keys.getString(Tag.StudyDate),
												null, 0, 0,
												"Error: Not found queries."));
										getStatistics(QueryStatistics.failed, 0, 0, qrConfig.getFailedFileName());
									} else
										//Case of choose -q option from command line
										LOG.error("Not found query with this query parameters");
									break;
								}
							} else {
								result = Collections.singletonList(getKeys());
							}
							if (isCMove() || isCGet()) {	
									if (isCMove())
										move(result);
									else
										get(result);
								long t4 = System.currentTimeMillis();
								LOG.info(
										"Retrieved {} objects (warning: {}, failed: {}) in {}s",
										new Object[] {
												Integer.valueOf(getTotalRetrieved()),
												Integer.valueOf(getWarning()),
												Integer.valueOf(getFailed()),
												Float.valueOf((t4 - t2) / 1000f) });
								if(getTotalRetrieved() == 0){
									if(!keys.getString(Tag.PatientID).isEmpty() || !keys.getString(Tag.StudyDate).isEmpty()){
										LOG.error("Unable to retrieve any study from query {} with Patient ID {} and Study Date {}",
												new Object [] {Integer.valueOf(i+1),
													keys.getString(Tag.PatientID), 
													DateTimeUtils.formatDate(keys.getString(Tag.StudyDate))});
										qPendingToStore.add(new QueryStatus(null,
												keys.getString(Tag.PatientID),
												keys.getString(Tag.StudyDate),
												null, null, null,
												"Error: Not found studies."));
										getStatistics(QueryStatistics.failed, 0, 0, qrConfig.getFailedFileName());
									} else
										//Case of choose -q option from command line
										LOG.error("Unable to retrieve any study with this query parameters");
								}
							} 
						
							if (repeat == 0 || closeAssoc) {
								try {
									close();
								} catch (InterruptedException e) {
									LOG.error(e.getMessage(), e);
								}
								LOG.info("Released connection to {}", stringRemoteAET);
							}
							if (repeat == 0)
								break;
							Thread.sleep(interval);
							long t4 = System.currentTimeMillis();
							open();
							t2 = System.currentTimeMillis();
							LOG.info("Reconnect or reuse connection to {} in {} s",
									stringRemoteAET, Float.valueOf((t2 - t4) / 1000f));
						}
						
						LOG.info("Waiting for the next query");
						//Clean qPendingToStore to avoid repeated studies in out file
						qPendingToStore.clear();
						break;
					
					} catch (IOException e) {
						LOG.error(e.getMessage(), e);
						LOG.error("Open() Exception. Failed query {} with Patient ID {} and Study Date {}",
								new Object [] {Integer.valueOf(i+1),patients.get(i),studyDates.get(i)});
						
					} catch (InterruptedException e) {
						LOG.error(e.getMessage(), e);
						LOG.error("Open() Exception. Failed query {} with Patient ID {} and Study Date {}",
								new Object [] {Integer.valueOf(i+1),patients.get(i),studyDates.get(i)});
					} catch (ConfigurationException e) {
						LOG.error(e.getMessage(), e);
						LOG.error("Open() Exception. Failed query {} with Patient ID {} and Study Date {}",
								new Object [] {Integer.valueOf(i+1),patients.get(i),studyDates.get(i)});
					} 
				}
			}			
			
			/*Stop listening*/
			stop();
			
			LOG.info("End of analysis");
			LOG.info("Socket Closed. Summary: Retrieved {} objects (warning: {}, failed: {})",
				new Object[] { Integer.valueOf(getTotalRetrieved()), Integer.valueOf(getWarning()),	Integer.valueOf(getFailed())});
		}else
			LOG.error("There are not parameters to start the analysis.");
	}
	
	private void getStatistics(Map<String,ArrayList<String>> statistics, int qId, int failed, String fileName){
		String studyiuid = qPendingToStore.get(qId).getStudyIuid();
		ArrayList<String> studyInfo = new ArrayList<String>();
		studyInfo.add(qPendingToStore.get(qId).getPatientId());
		studyInfo.add(DateTimeUtils.formatDate(qPendingToStore.get(qId).getStudyDate()));
		studyInfo.add(String.valueOf(qPendingToStore.get(qId).getTotalInstances()));
		studyInfo.add(String.valueOf(failed));
		
		for(int i = 0; i < qPendingToStore.get(qId).getInfo().size(); i++)
			studyInfo.add(qPendingToStore.get(qId).getInfo().get(i));
			
		statistics.put(studyiuid, studyInfo);
		QueryStatistics.extractStatistics(statistics, fileName);
		
		if(studyiuid == null)
			studyiuid = qPendingToStore.get(qId).getPatientId() + "-"
				+DateTimeUtils.formatDate(qPendingToStore.get(qId).getStudyDate());
		
		qProcessed.put(studyiuid, qPendingToStore.get(qId).getMoveRQId());
		
		//Clean them to avoid repeated studies in out file
		statistics.clear();
		qPendingToStore.clear();
	}
	
	public void configureDcmQr(DcmQrConfig config){
		setQrConfig(config);
		
		setCalledAET(config.getCalledAET(), false);
		setRemoteHost(config.getRemoteHost());
		setRemotePort(toPort(config.getRemotePort()));
	
		setLocalPort(toPort(config.getLocalPort()));
		setCalling(config.getCallingAET());
		setLocalHost(config.getLocalHost());
		
		String[] storeTCs = new String[] { 
				config.getModality(), "PR:LE" };
		
		for (String storeTC : storeTCs) {
			String cuid;
			String[] tsuids;
			int colon = storeTC.indexOf(':');
			if (colon == -1) {
				cuid = storeTC;
				tsuids = DEF_TS;
			} else {
				cuid = storeTC.substring(0, colon);
				String ts = storeTC.substring(colon + 1);
				try {
					tsuids = TS.valueOf(ts).uids;
				} catch (IllegalArgumentException e) {
					tsuids = ts.split(",");
				}
			}
			try {
				cuid = CUID.valueOf(cuid).uid;
			} catch (IllegalArgumentException e) {
				// assume cuid already contains UID
			}
			
			addStoreTransferCapability(cuid, tsuids);
		}
		
		setStoreDestination(config.getStoreDestPath());
		
		if (qrConfig.getConnectTimeout() != 0) setConnectTimeout(qrConfig.getConnectTimeout());
		if (qrConfig.getAssociationReaperPeriod() != 0) setAssociationReaperPeriod(qrConfig.getAssociationReaperPeriod());
		if (qrConfig.getDimseRspTimeout() != 0) setDimseRspTimeout(qrConfig.getDimseRspTimeout());
		if (qrConfig.getRetrieveRspTimeout() != 0) setRetrieveRspTimeout(qrConfig.getRetrieveRspTimeout());
		if (qrConfig.getAcceptTimeout() != 0) setAcceptTimeout(qrConfig.getAcceptTimeout());
		if (qrConfig.getReleaseTimeout() != 0) setReleaseTimeout(qrConfig.getReleaseTimeout());
		if (qrConfig.getSocketCloseDelay() != 0) setSocketCloseDelay(qrConfig.getSocketCloseDelay());
		if (qrConfig.getMaxPDULengthReceive() != 0) setMaxPDULengthReceive(qrConfig.getMaxPDULengthReceive());
		if (qrConfig.getMaxPDULengthSend() != 0) setMaxPDULengthSend(qrConfig.getMaxPDULengthSend());
		if (qrConfig.getSendBufferSize() != 0) setSendBufferSize(qrConfig.getSendBufferSize());
		if (qrConfig.getReceiveBufferSize() != 0) setReceiveBufferSize(qrConfig.getReceiveBufferSize());
		if (qrConfig.getMaxOpsInvoked() != 0) setMaxOpsInvoked(qrConfig.getMaxOpsInvoked());
		if (qrConfig.getMaxOpsPerformed() != 0) setMaxOpsPerformed(qrConfig.getMaxOpsPerformed());
	
		if(config.getQrLevel() == null)
			setQueryLevel(QueryRetrieveLevel.STUDY);
		else if(config.getQrLevel().equals("P"))
			setQueryLevel(QueryRetrieveLevel.PATIENT);
		else if(config.getQrLevel().equals("S"))
			setQueryLevel(QueryRetrieveLevel.SERIES);
		else if(config.getQrLevel().equals("I"))
			setQueryLevel(QueryRetrieveLevel.IMAGE);

		if (config.getQrLevel() != null && !config.getQrLevel().equals("P")) {
			if (config.getRetall())
					addPrivate(UID.PrivateStudyRootQueryRetrieveInformationModelFIND);
			if (config.getBlocked())
					addPrivate(UID.PrivateBlockedStudyRootQueryRetrieveInformationModelFIND);
			if (config.getVmf())
					addPrivate(UID.PrivateVirtualMultiframeStudyRootQueryRetrieveInformationModelFIND);
		}
		
		if (isCFind()) {
			String[] cuids = qrConfig.getCuids();
			if(cuids != null)
				for (int i = 0; i < cuids.length; i++)
					addPrivate(cuids[i]);
		}
		
		if (config.getInputData() && !config.getqParams()){	
			String ifile = config.getInFileName();
			try {
				if(ifile != null)
					if(ifile.endsWith(".csv"))
						readIncomingFile(ifile);
			} catch (IOException e) {
				LOG.error("Cannot read file. It must be a .csv file.");
			}	
		}
		
		if (!config.getInputData() && config.getqParams()) {
			String[] matchingKeys = config.getMatchingKeys();
			for (int i = 1; i < matchingKeys.length; i++, i++){
				addMatchingKey(Tag.toTagPath(matchingKeys[i - 1]),
						matchingKeys[i]);
//				if(matchingKeys[i-1].equals("00100020")) /*Patient ID */
//					currentQuery[0] = matchingKeys[i];
//				if(matchingKeys[i-1].equals("00080020")) /*Study Date */
//					currentQuery[1] = matchingKeys[i];
			}
		}
		
		if (config.getOutputData()){		
			if(config.getOutFileName() != null) {
				if(!config.getOutFileName().endsWith(".csv")){
						LOG.error("Cannot write file. It must be a .csv file.");
						System.exit(2);
				}
			} 
		} else if(config.getOutFileName() == null || !config.getOutputData())
			qrConfig.setOutFileName(config.getModality()+"-"+DateTimeUtils.getCurrentDateTime()+".csv");
		
		if (config.getrParams()) {
			String[] returnKeys = config.getReturnKeys();
			for (int i = 0; i < returnKeys.length; i++)
				addReturnKey(Tag.toTagPath(returnKeys[i]));
		}
		 
		configureTransferCapability(qrConfig.getIvrleTC());
		
		if(qrConfig.getTls()){
			setTlsProtocol(qrConfig.getTlsProtocol());
			String cipher = qrConfig.getCipherTls();
			if ("NULL".equalsIgnoreCase(cipher)) setTlsWithoutEncyrption();
	        else if ("3DES".equalsIgnoreCase(cipher)) setTls3DES_EDE_CBC();
	        else if ("AES".equalsIgnoreCase(cipher)) setTlsAES_128_CBC();
	        else DicomtomyDose.exit("Invalid parameter for option -tls: " + cipher);
			
			long t1 = System.currentTimeMillis();
	        
	        try {
	            initTLS();
	        } catch (Exception e) {
	            System.err.println("ERROR: Failed to initialize TLS context:"
	                    + e.getMessage());
	            System.exit(2);
	        }
	        
	        long t2 = System.currentTimeMillis();
	        LOG.info("Initialize TLS context in {} s", Float.valueOf((t2 - t1) / 1000f));
		}
	}
	
	public void configureTransferCapability(boolean ivrle) {
		String[] findcuids = qrlevel.getFindClassUids();
		String[] movecuids = qrConfig.getMovedest() != null ? qrlevel.getMoveClassUids()
				: EMPTY_STRING;
		String[] getcuids = qrConfig.getCget() ? qrlevel.getGetClassUids() : EMPTY_STRING;
		TransferCapability[] tcs = new TransferCapability[findcuids.length
				+ privateFind.size() + movecuids.length + getcuids.length
				+ storeTransferCapability.size()];
		int i = 0;
		for (String cuid : findcuids)
			tcs[i++] = mkFindTC(cuid, ivrle ? IVRLE_TS : NATIVE_LE_TS);
		for (String cuid : privateFind)
			tcs[i++] = mkFindTC(cuid, ivrle ? IVRLE_TS : DEFLATED_TS);
		for (String cuid : movecuids)
			tcs[i++] = mkRetrieveTC(cuid, ivrle ? IVRLE_TS : NATIVE_LE_TS);
		for (String cuid : getcuids)
			tcs[i++] = mkRetrieveTC(cuid, ivrle ? IVRLE_TS : NATIVE_LE_TS);
		for (TransferCapability tc : storeTransferCapability) {
			tcs[i++] = tc;
		}
		ae.setTransferCapability(tcs);
		if (!storeTransferCapability.isEmpty()) {
			ae.register(createStorageService());
		}
	}
	
	protected DicomService createStorageService() {
		String[] cuids = new String[storeTransferCapability.size()];
		int i = 0;
		for (TransferCapability tc : storeTransferCapability) {
			cuids[i++] = tc.getSopClass();
		}
		return new StorageService(cuids) {
			@Override
			protected void onCStoreRQ(Association as, int pcid, DicomObject rq,
					PDVInputStream dataStream, String tsuid, DicomObject rsp)
					throws IOException, DicomServiceException {		
				int qId = 0;
				
				try {
					String cuid = rq.getString(Tag.AffectedSOPClassUID);
					String iuid = rq.getString(Tag.AffectedSOPInstanceUID);
					
					BasicDicomObject fmi = new BasicDicomObject();
					fmi.initFileMetaInformation(cuid, iuid, tsuid);

					File ifile = new File(storeDest, iuid);
					FileOutputStream fos = new FileOutputStream(ifile);
					BufferedOutputStream bos = new BufferedOutputStream(fos,
							qrConfig.getFileBufferSize());
					DicomOutputStream dos = new DicomOutputStream(bos);
					dos.writeFileMetaInformation(fmi);
					
					dataStream.copyTo(dos);
//					String moveRQiDintoRQ = rq.getString(Tag.MessageIDBeingRespondedTo);
					dos.close();
					
					fmi = new BasicDicomObject();
					DicomInputStream dis = new DicomInputStream(ifile);
					dis.readDicomObject(fmi, -1);

					String auxHeader, auxInfo, studyIuid = null;
															
					/*Matching DICOM Object with its study instance UID to get its number of instances and decrease it*/
					if(fulfillModalityRequirements(fmi)){	
						Integer rInstances = null;		
												
						for(qId = 0; qId < qPendingToStore.size(); qId++)
						{
//							/*Avoid to do cstore of a study that have already been processed before */
//							if(moveRQiDintoRQ.equals(qPendingToStore.get(qId).getMoveRQId()) && isqProcessedStudy(fmi)) {
//								as.abort();
//								break;
//							}
							
							studyIuid = qPendingToStore.get(qId).getStudyIuid();
							
							if(fmi.getString(Tag.StudyInstanceUID).equals(studyIuid) ){
								rInstances = qPendingToStore.get(qId).getRemainingInstances();
								--rInstances;
								qPendingToStore.get(qId).setRemainingInstances(rInstances);							
																
								if(activeHeaders){
									auxHeader = (OutFormatUtils.prepareHeaderFile(fmi, activeHeaders) 
											+ prepareModalityHeader(fmi, ifile, activeHeaders) +"\n"); /*End of headers */
									qPendingToStore.get(qId).addInfo(auxHeader);
								}
								
								auxInfo = ((OutFormatUtils.prepareCommonValues(fmi, ifile) 
										+ prepareModalityValues(fmi, ifile))+"\n");/*End of Values */
								qPendingToStore.get(qId).addInfo(auxInfo);
							}
						}
						
						/*Reset flags*/
						activeHeaders = false;
						imageAnalysis = false;
						newQuery = false;
					}
									
					/*Save manufacturer model name of the processed image (check out repeated headers) */
					currentEquipment = fmi.getString(Tag.ManufacturerModelName);
					
					/* Close input stream (image DICOM) and  output stream (temporary file). 
					 * Delete temporaryabortSTorage file. */
					dis.close();
					fmi.clear();
					ifile.delete();
					fos.flush();
					fos.close();
				
				} catch (IOException dse) {
					ArrayList<String> errorMsg = new ArrayList<String>();					
					QueryStatus qFailed = qPendingToStore.get(qId);
					errorMsg.add("Error : Aborted Storage! Something is wrong with this image. "+dse.getMessage());
					qFailed.setInfo(errorMsg);
					qPendingToStore.set(qId, qFailed);
					getStatistics(QueryStatistics.failed, 0, -1, qrConfig.getFailedFileName());
					qProcessed.put(qPendingToStore.get(qId).getStudyIuid(),qPendingToStore.get(qId).getMoveRQId());
					throw new DicomServiceException(rq,
							Status.ProcessingFailure, dse.getMessage());
				}
			}
		};
		
		
	}
		
	protected TransferCapability mkRetrieveTC(String cuid, String[] ts) {
		ExtRetrieveTransferCapability tc = new ExtRetrieveTransferCapability(
				cuid, ts, TransferCapability.SCU);
		tc.setExtInfoBoolean(
				ExtRetrieveTransferCapability.RELATIONAL_RETRIEVAL, qrConfig.getRelationQR());
		if (qrConfig.getNotExtNegociation())
			tc.setExtInfo(null);
		return tc;
	}

	protected TransferCapability mkFindTC(String cuid, String[] ts) {
		ExtQueryTransferCapability tc = new ExtQueryTransferCapability(cuid,
				ts, TransferCapability.SCU);
		tc.setExtInfoBoolean(ExtQueryTransferCapability.RELATIONAL_QUERIES,
				qrConfig.getRelationQR());
		tc.setExtInfoBoolean(ExtQueryTransferCapability.DATE_TIME_MATCHING,
				qrConfig.getDateTimeMatching());
		tc.setExtInfoBoolean(
				ExtQueryTransferCapability.FUZZY_SEMANTIC_PN_MATCHING,
				qrConfig.getFuzzySemanticPersonNameMatching());
		if (qrConfig.getNotExtNegociation())
			tc.setExtInfo(null);
		return tc;
	}

	public void setQueryLevel(QueryRetrieveLevel qrlevel) {
		this.qrlevel = qrlevel;
		keys.putString(Tag.QueryRetrieveLevel, VR.CS, qrlevel.getCode());
		for (int tag : qrlevel.getReturnKeys()) {
			keys.putNull(tag, null);
		}
	}

	public final void addPrivate(String cuid) {
		privateFind.add(cuid);
	}

	public boolean isCFind() {
		return qrConfig.getCfind();
	}

	public boolean isCGet() {
		return qrConfig.getCget();
	}

	public boolean isCMove() {
		return qrConfig.getMovedest() != null;
	}

	protected static int toPort(String port) {
		return port != null ? parseInt(port, "illegal port number", 1, 0xffff)
				: 104;
	}

	protected static int parseInt(String s, String errPrompt, int min, int max) {
		try {
			int i = Integer.parseInt(s);
			if (i >= min && i <= max)
				return i;
		} catch (NumberFormatException e) {
			// parameter is not a valid integer; fall through to exit
		}
		exit(errPrompt);
		throw new RuntimeException();
	}

	protected static String[] split(String s, char delim) {
		String[] s2 = { s, null };
		int pos = s.indexOf(delim);
		if (pos != -1) {
			s2[0] = s.substring(0, pos);
			s2[1] = s.substring(pos + 1);
		}
		return s2;
	}

	protected static void exit(String msg) {
		System.err.println(msg);
		System.err.println("Try 'dcmAnalisis -h' for more information.");
		System.exit(1);
	}

	public void start() throws IOException {
		if (conn.isListening()) {
			conn.bind(executor);
		}
	}

	public void stop() {
		if (conn.isListening()) {
			conn.unbind();
		}
	}

	public void open() throws IOException, ConfigurationException,
			InterruptedException {
		assoc = ae.connect(remoteAE, executor);
	}

	public DicomObject getKeys() {
		return keys;
	}

	public List<DicomObject> query() throws IOException, InterruptedException {
		List<DicomObject> result = new ArrayList<DicomObject>();
		TransferCapability tc = selectFindTransferCapability();
		String cuid = tc.getSopClass();
		String tsuid = selectTransferSyntax(tc);
		
		if (tc.getExtInfoBoolean(ExtQueryTransferCapability.RELATIONAL_QUERIES)
				|| containsUpperLevelUIDs(cuid)) {
			LOG.info("Send Query Request using {}:\n{}", UIDDictionary
					.getDictionary().prompt(cuid), keys);
			
			DimseRSP rsp = assoc
					.cfind(cuid, qrConfig.getPriority(), keys, tsuid, qrConfig.getCancelAfter());
			while (rsp.next()) {
				DicomObject cmd = rsp.getCommand();
				if (CommandUtils.isPending(cmd)) {
					DicomObject data = rsp.getDataset();
					
					/*Avoid repeated results (that is, with same Study IUID) */
					removeRepeatQueries(result, data);
				}
			}
		} else {
			List<DicomObject> upperLevelUIDs = queryUpperLevelUIDs(cuid, tsuid);
			List<DimseRSP> rspList = new ArrayList<DimseRSP>(
					upperLevelUIDs.size());
			for (int i = 0, n = upperLevelUIDs.size(); i < n; i++) {
				upperLevelUIDs.get(i).copyTo(keys);
				LOG.info(
						"Send Query Request #{}/{} using {}:\n{}",
						new Object[] { Integer.valueOf(i + 1),
								Integer.valueOf(n),
								UIDDictionary.getDictionary().prompt(cuid),
								keys });
				rspList.add(assoc.cfind(cuid, qrConfig.getPriority(), keys, tsuid,
						qrConfig.getCancelAfter()));
			}
			for (int i = 0, n = rspList.size(); i < n; i++) {
				DimseRSP rsp = rspList.get(i);
				for (int j = 0; rsp.next(); ++j) {
					DicomObject cmd = rsp.getCommand();
					if (CommandUtils.isPending(cmd)) {
						DicomObject data = rsp.getDataset();
						
						/*Avoid repeated results (that is, with same Study IUID) */
						removeRepeatQueries(result, data);
					}
				}
			}
		}
		
		if(!result.isEmpty())
			for (int i = 0; i < result.size(); i++)
				if(isqProcessedStudy(result.get(i)))
					result.remove(i);
				else
					LOG.info("Query Response #{}:\n{}",
						Integer.valueOf(i+1), result.get(i));	
			
		return result;
	}
	
	/*Avoid repeated results (that is, with same Study IUID) */
	private void removeRepeatQueries (List<DicomObject> findResults, DicomObject data){	
		String auxIuid;
		Integer auxInstances;
		Boolean bestResult = true;
		
		String newStudyIUID = data.getString(Tag.StudyInstanceUID);
		Integer newTotalInstances = Integer.valueOf(data.getInt(Tag.NumberOfStudyRelatedInstances));
		
		if(findResults != null && !findResults.isEmpty()){
			for (int k = 0; k < findResults.size(); k++){	
				auxIuid = findResults.get(k).getString(Tag.StudyInstanceUID);
				auxInstances = findResults.get(k).getInt(Tag.NumberOfStudyRelatedInstances);
				if(newStudyIUID.equals(auxIuid)){
					if (newTotalInstances > auxInstances){
						findResults.remove(k);
						bestResult = true; 
						
					} else {
						/* There is a best result than the new data. 
						 * The search of repeated queries ends. */
						bestResult = false; 
						break;
					} 
				} 
			}
		}
	
		/* Add the new data if the list is empty or is the best result and 
		 * if study haven't been processed yet*/
		if(bestResult) { findResults.add(data);}
		
	}
	
	/*Avoid to move studies that have already been processed */
	@SuppressWarnings("rawtypes")
	private Boolean isqProcessedStudy (DicomObject data){
		Boolean processed = false;
		
		if(data != null){
			String studyIUID =  data.getString(Tag.StudyInstanceUID);
			
			if(!qProcessed.isEmpty()){
				Iterator it = qProcessed.entrySet().iterator();
				
				while (it.hasNext()){
					Map.Entry e = (Map.Entry) it.next();
					String processedId = (String) e.getKey();
					
					if(processedId.equals(studyIUID)){
						qPendingToStore.add(new QueryStatus(studyIUID, 
								keys.getString(Tag.PatientID), 
								keys.getString(Tag.StudyDate),
								null, null, null, "Error: This study has already been processed."));
						getStatistics(QueryStatistics.failed, 0, 0, qrConfig.getFailedFileName());
						processed = true;
					}
				}
			}
		}
		return processed;
	}

	@SuppressWarnings("fallthrough")
	protected boolean containsUpperLevelUIDs(String cuid) {
		switch (qrlevel) {
		case IMAGE:
			if (!keys.containsValue(Tag.SeriesInstanceUID)) {
				return false;
			}
			// fall through
		case SERIES:
			if (!keys.containsValue(Tag.StudyInstanceUID)) {
				return false;
			}
			// fall through
		case STUDY:
			if (Arrays.asList(PATIENT_LEVEL_FIND_CUID).contains(cuid)
					&& !keys.containsValue(Tag.PatientID)) {
				return false;
			}
			// fall through
		case PATIENT:
			// fall through
		}
		return true;
	}

	protected List<DicomObject> queryUpperLevelUIDs(String cuid, String tsuid)
			throws IOException, InterruptedException {
		List<DicomObject> keylist = new ArrayList<DicomObject>();
		if (Arrays.asList(PATIENT_LEVEL_FIND_CUID).contains(cuid)) {
			queryPatientIDs(cuid, tsuid, keylist);
			if (qrlevel == QueryRetrieveLevel.STUDY) {
				return keylist;
			}
			keylist = queryStudyOrSeriesIUIDs(cuid, tsuid, keylist,
					Tag.StudyInstanceUID, STUDY_MATCHING_KEYS,
					QueryRetrieveLevel.STUDY);
		} else {
			keylist.add(new BasicDicomObject());
			keylist = queryStudyOrSeriesIUIDs(cuid, tsuid, keylist,
					Tag.StudyInstanceUID, PATIENT_STUDY_MATCHING_KEYS,
					QueryRetrieveLevel.STUDY);
		}
		if (qrlevel == QueryRetrieveLevel.IMAGE) {
			keylist = queryStudyOrSeriesIUIDs(cuid, tsuid, keylist,
					Tag.SeriesInstanceUID, SERIES_MATCHING_KEYS,
					QueryRetrieveLevel.SERIES);
		}
		return keylist;
	}

	protected void queryPatientIDs(String cuid, String tsuid,
			List<DicomObject> keylist) throws IOException, InterruptedException {
		String patID = keys.getString(Tag.PatientID);
		String issuer = keys.getString(Tag.IssuerOfPatientID);
		if (patID != null) {
			DicomObject patIdKeys = new BasicDicomObject();
			patIdKeys.putString(Tag.PatientID, VR.LO, patID);
			if (issuer != null) {
				patIdKeys.putString(Tag.IssuerOfPatientID, VR.LO, issuer);
			}
			keylist.add(patIdKeys);
		} else {
			DicomObject patLevelQuery = new BasicDicomObject();
			keys.subSet(PATIENT_MATCHING_KEYS).copyTo(patLevelQuery);
			patLevelQuery.putNull(Tag.PatientID, VR.LO);
			patLevelQuery.putNull(Tag.IssuerOfPatientID, VR.LO);
			patLevelQuery.putString(Tag.QueryRetrieveLevel, VR.CS, "PATIENT");
			LOG.info("Send Query Request using {}:\n{}", UIDDictionary
					.getDictionary().prompt(cuid), patLevelQuery);
			DimseRSP rsp = assoc.cfind(cuid, qrConfig.getPriority(), patLevelQuery, tsuid,
					Integer.MAX_VALUE);
			for (int i = 0; rsp.next(); ++i) {
				DicomObject cmd = rsp.getCommand();
				if (CommandUtils.isPending(cmd)) {
					DicomObject data = rsp.getDataset();
					LOG.info("Query Response #{}:\n{}", Integer.valueOf(i + 1),
							data);
					DicomObject patIdKeys = new BasicDicomObject();
					patIdKeys.putString(Tag.PatientID, VR.LO,
							data.getString(Tag.PatientID));
					issuer = keys.getString(Tag.IssuerOfPatientID);
					if (issuer != null) {
						patIdKeys.putString(Tag.IssuerOfPatientID, VR.LO,
								issuer);
					}
					keylist.add(patIdKeys);
				}
			}
		}
	}

	protected List<DicomObject> queryStudyOrSeriesIUIDs(String cuid,
			String tsuid, List<DicomObject> upperLevelIDs, int uidTag,
			int[] matchingKeys, QueryRetrieveLevel qrLevel) throws IOException,
			InterruptedException {
		List<DicomObject> keylist = new ArrayList<DicomObject>();
		String uid = keys.getString(uidTag);
		for (DicomObject upperLevelID : upperLevelIDs) {
			if (uid != null) {
				DicomObject suidKey = new BasicDicomObject();
				upperLevelID.copyTo(suidKey);
				suidKey.putString(uidTag, VR.UI, uid);
				keylist.add(suidKey);
			} else {
				DicomObject keys2 = new BasicDicomObject();
				keys.subSet(matchingKeys).copyTo(keys2);
				upperLevelID.copyTo(keys2);
				keys2.putNull(uidTag, VR.UI);
				keys2.putString(Tag.QueryRetrieveLevel, VR.CS,
						qrLevel.getCode());
				LOG.info("Send Query Request using {}:\n{}", UIDDictionary
						.getDictionary().prompt(cuid), keys2);
				DimseRSP rsp = assoc.cfind(cuid, qrConfig.getPriority(), keys2, tsuid,
						Integer.MAX_VALUE);
				for (int i = 0; rsp.next(); ++i) {
					DicomObject cmd = rsp.getCommand();
					if (CommandUtils.isPending(cmd)) {
						DicomObject data = rsp.getDataset();
						LOG.info("Query Response #{}:\n{}",
								Integer.valueOf(i + 1), data);
						DicomObject suidKey = new BasicDicomObject();
						upperLevelID.copyTo(suidKey);
						suidKey.putString(uidTag, VR.UI, data.getString(uidTag));
						keylist.add(suidKey);
					}
				}
			}
		}
		return keylist;
	}

	public TransferCapability selectFindTransferCapability()
			throws NoPresentationContextException {
		TransferCapability tc;
		if ((tc = selectTransferCapability(privateFind)) != null)
			return tc;
		if ((tc = selectTransferCapability(qrlevel.getFindClassUids())) != null)
			return tc;
		throw new NoPresentationContextException(UIDDictionary.getDictionary()
				.prompt(qrlevel.getFindClassUids()[0])
				+ " not supported by "
				+ remoteAE.getAETitle());
	}

	public String selectTransferSyntax(TransferCapability tc) {
		String[] tcuids = tc.getTransferSyntax();
		if (Arrays.asList(tcuids).indexOf(UID.DeflatedExplicitVRLittleEndian) != -1)
			return UID.DeflatedExplicitVRLittleEndian;
		return tcuids[0];
	}

	public void move(List<DicomObject> findResults) throws IOException,
			InterruptedException {		
		if (qrConfig.getMovedest() == null)
			throw new IllegalStateException("moveDest == null");
		TransferCapability tc = selectTransferCapability(qrlevel
				.getMoveClassUids());
		if (tc == null)
			throw new NoPresentationContextException(UIDDictionary
					.getDictionary().prompt(qrlevel.getMoveClassUids()[0])
					+ " not supported by " + remoteAE.getAETitle());
		String cuid = tc.getSopClass();
		String tsuid = selectTransferSyntax(tc);
		
		for (int i = 0, n = Math.min(findResults.size(), qrConfig.getCancelAfter()); i < n; ++i) {
			DicomObject keys = findResults.get(i).subSet(MOVE_KEYS);
			newQuery = true;
			
			if (isEvalRetrieveAET()
					&& containsMoveDest(findResults.get(i).getStrings(
							Tag.RetrieveAETitle))) {
				LOG.info("Skipping {}:\n{}", UIDDictionary.getDictionary()
						.prompt(cuid), keys);
			} else {			
				LOG.info("Send Retrieve Request using {}:\n{}", UIDDictionary
						.getDictionary().prompt(cuid), keys);	
				assoc.cmove(cuid, qrConfig.getPriority(), keys, tsuid, qrConfig.getMovedest(), rspHandler);
				
				/*Get study instance UID and its related instances */
				String studyIUID, patientId, studyDate;
				Integer moveRqMsgID, totalInstances, remaining;
				
				studyIUID = findResults.get(i).getString(Tag.StudyInstanceUID);
				patientId = findResults.get(i).getString(Tag.PatientID);
				studyDate = findResults.get(i).getString(Tag.StudyDate);
				moveRqMsgID = (Integer) rspHandler.getMessageID(); /*C-MOVE-RQ message ID*/
				totalInstances = remaining = findResults.get(i).getInt(Tag.NumberOfStudyRelatedInstances); 

				//Add to list of queries pending to store
				qPendingToStore.add(new QueryStatus(studyIUID, patientId, studyDate,
					moveRqMsgID, totalInstances, remaining));
			}
		}
		assoc.waitForDimseRSP();
	}
	
	protected boolean containsMoveDest(String[] retrieveAETs) {
		if (retrieveAETs != null) {
			for (String aet : retrieveAETs) {
				if (qrConfig.getMovedest().equals(aet)) {
					return true;
				}
			}
		}
		return false;
	}

	public void get(List<DicomObject> findResults) throws IOException,
			InterruptedException {
		TransferCapability tc = selectTransferCapability(qrlevel
				.getGetClassUids());
		if (tc == null)
			throw new NoPresentationContextException(UIDDictionary
					.getDictionary().prompt(qrlevel.getGetClassUids()[0])
					+ " not supported by " + remoteAE.getAETitle());
		String cuid = tc.getSopClass();
		String tsuid = selectTransferSyntax(tc);
		for (int i = 0, n = Math.min(findResults.size(), qrConfig.getCancelAfter()); i < n; ++i) {
			DicomObject keys = findResults.get(i).subSet(MOVE_KEYS);
			LOG.info("Send Retrieve Request using {}:\n{}", UIDDictionary
					.getDictionary().prompt(cuid), keys);
			assoc.cget(cuid, qrConfig.getPriority(), keys, tsuid, rspHandler);
		}
		assoc.waitForDimseRSP();
	}

	protected final DimseRSPHandler rspHandler = new DimseRSPHandler() {
		@Override
		public void onDimseRSP(Association as, DicomObject cmd, DicomObject data) {
			try {
				DcmQr.this.onMoveRSP(as, cmd, data);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	};
	
	protected void onMoveRSP(Association as, DicomObject cmd, DicomObject data) throws InterruptedException {		
		currentCMoveRspID = Integer.valueOf(cmd.getInt(Tag.MessageIDBeingRespondedTo));
		int failedForCurrentRSP = cmd.getInt(Tag.NumberOfFailedSuboperations);
		String completedForCurrentRSP = cmd.getString(Tag.NumberOfCompletedSuboperations);
		int remainingForCurrentRSP = cmd.getInt(Tag.NumberOfRemainingSuboperations);
		
		if (!CommandUtils.isPending(cmd)) {
			completed += cmd.getInt(Tag.NumberOfCompletedSuboperations);
			warning += cmd.getInt(Tag.NumberOfWarningSuboperations);
			failed += cmd.getInt(Tag.NumberOfFailedSuboperations);
		}

		if(D4CUtils.isWarning(cmd))
		{
			Boolean partialSuccess = false;
			
			for(int qId = 0; qId < qPendingToStore.size(); qId++)
			{		
				String studyIuid = qPendingToStore.get(qId).getStudyIuid();
				Integer totalImages = qPendingToStore.get(qId).getTotalInstances();
				Integer myRemaining = qPendingToStore.get(qId).getRemainingInstances();
				
				if(currentCMoveRspID.equals(qPendingToStore.get(qId).getMoveRQId()) 
						&& !qProcessed.containsKey(studyIuid)){
					
					/*This occurs when the study's modality is different from modality selected by the user */
					if(completedForCurrentRSP == null){
						ArrayList<String> errorMsg = new ArrayList<String>();
						errorMsg.add("Error: Study with a different modality");
						qPendingToStore.get(qId).setInfo(errorMsg);
					}
					
					/*Troublesome study. Catches this exception and continue with next study if exists 
					 * or ???? maybe the study is valid (with 75% or more of images completed.)*/
					if(completedForCurrentRSP != null && failedForCurrentRSP > 0){
						//For current C-MOVE-RSP, calculates 'x' percent of completed images and gets total of instances 
						int total = remainingForCurrentRSP + Integer.parseInt(completedForCurrentRSP) + failedForCurrentRSP;
						int completedPercent = (Integer.parseInt(completedForCurrentRSP)*100)/total;
						
						if(completedPercent >= 75 && myRemaining >= 0 && myRemaining < totalImages){
							partialSuccess = hasDoseInfo(qId);
							qPendingToStore.get(qId).addInfo("Partial Success: "+completedPercent+"% completed");
							
						} else {
							ArrayList<String> errorMsg = new ArrayList<String>();
							errorMsg.add("Error: Troublesome study!! association ends abruptly");
							qPendingToStore.get(qId).setInfo(errorMsg);
						}
					}
					
					//Add Troublesome or Successful Studies
					if(partialSuccess)
						getStatistics(QueryStatistics.succesful, qId, failedForCurrentRSP, qrConfig.getSuccessFileName());
					else
						getStatistics(QueryStatistics.failed, qId, failedForCurrentRSP, qrConfig.getFailedFileName());
				}
			}
			
		}
		
		//Write file each time that happens a success c-move-rsp or a warning c-move-rsp with completed=null studies
		if(D4CUtils.isSuccess(cmd)){
			Boolean doseFound = false;
			
			for(int qId = 0; qId < qPendingToStore.size(); qId++){
				String studyIuid = qPendingToStore.get(qId).getStudyIuid();
				Integer totalImages = qPendingToStore.get(qId).getTotalInstances();
				Integer myRemaining = qPendingToStore.get(qId).getRemainingInstances();
				
				if(!qProcessed.containsKey(studyIuid)){
					/*There have had instances that fulfill the requirements */
					if(myRemaining < totalImages){
						if(myRemaining < 0) qPendingToStore.get(qId).setTotalInstances(totalImages-myRemaining);
						doseFound = hasDoseInfo(qId);
						
					} else{
						/*Add Failed Studies 
						 * Studies that it does not fulfill with requirements, for example,
						 * those that have not dose info like little CT Brilliance studies cause they have not summary dose */
							ArrayList<String> errorMsg = new ArrayList<String>();
							errorMsg.add("Error: There is not dose information of interest");
							qPendingToStore.get(qId).setInfo(errorMsg);
					}
				}
				//Add Troublesome or Successful Studies
				if(doseFound)
					getStatistics(QueryStatistics.succesful, qId, failedForCurrentRSP, qrConfig.getSuccessFileName());
				else
					getStatistics(QueryStatistics.failed, qId, myRemaining, qrConfig.getFailedFileName());
			}
		}
	}
	
	private Boolean hasDoseInfo (int qId){
		Boolean doseFound = false;
		ArrayList<String> doseInfo = qPendingToStore.get(qId).getInfo();
		
		if(doseInfo != null && !doseInfo.isEmpty()){
			//Write dose information into out file
			for(int i = 0; i < doseInfo.size(); i++)
				writeOutcomingFile(ofile,doseInfo.get(i));
			doseFound = true;
			
			//Clean info for prepare after the content of statistic file
			qPendingToStore.get(qId).getInfo().clear();
			
		} else{
			QueryStatus status = qPendingToStore.get(qId);
			status.addInfo("Error: There is not information in this study");
			qPendingToStore.add(status);
		}	
		
		return doseFound;
	}

	public TransferCapability selectTransferCapability(String[] cuid) {
		TransferCapability tc;
		for (int i = 0; i < cuid.length; i++) {
			tc = assoc.getTransferCapabilityAsSCU(cuid[i]);
			if (tc != null)
				return tc;
		}
		return null;
	}

	public TransferCapability selectTransferCapability(List<String> cuid) {
		TransferCapability tc;
		for (int i = 0, n = cuid.size(); i < n; i++) {
			tc = assoc.getTransferCapabilityAsSCU(cuid.get(i));
			if (tc != null)
				return tc;
		}
		return null;
	}

	public void close() throws InterruptedException {
		assoc.release(true);
	}

	public void setStoreDestination(String filePath) {
		this.storeDest = new File(filePath);
		this.devnull = "/dev/null".equals(filePath);
		if (!devnull)
			storeDest.mkdir();
	}

	public void initTLS() throws GeneralSecurityException, IOException {
		System.out.println(qrConfig.getKeyStoreURL());
		System.out.println(qrConfig.getKeyStorePassword());
		System.out.println(qrConfig.getTrustStoreURL());
		System.out.println(qrConfig.getTrustStorePassword());
		System.out.println(qrConfig.getKeyPassword());
		
		KeyStore keyStore = loadKeyStore(qrConfig.getKeyStoreURL(), qrConfig.getKeyStorePassword());
		KeyStore trustStore = loadKeyStore(qrConfig.getTrustStoreURL(), qrConfig.getTrustStorePassword());
		device.initTLS(keyStore, qrConfig.getKeyPassword() != null ? qrConfig.getKeyPassword()
				: qrConfig.getKeyStorePassword(), trustStore);
	}

	protected static KeyStore loadKeyStore(String url, char[] password)
			throws GeneralSecurityException, IOException {
		KeyStore key = KeyStore.getInstance(toKeyStoreType(url));
		InputStream in = openFileOrURL(url);
		try {
			key.load(in, password);
		} finally {
			in.close();
		}
		return key;
	}

	protected static InputStream openFileOrURL(String url) throws IOException {
		if (url.startsWith("resource:")) {
			return DcmQr.class.getClassLoader().getResourceAsStream(
					url.substring(9));
		}
		try {
			return new URL(url).openStream();
		} catch (MalformedURLException e) {
			return new FileInputStream(url);
		}
	}

	protected static String toKeyStoreType(String fname) {
		return fname.endsWith(".p12") || fname.endsWith(".P12") ? "PKCS12"
				: "JKS";
	}
	
	protected abstract Boolean fulfillModalityRequirements(BasicDicomObject fmi);
	
	protected abstract String prepareModalityValues (BasicDicomObject fmi, File file);
	
	protected abstract String prepareModalityHeader (BasicDicomObject fmi, File file, Boolean active);
	
	private void prepareData (String studyFromPatient){
		/*The format for the studyFromPatient must be "<patient ID>|yyyy-mm-dd */
		int i = 0;
		
		try {
			StringTokenizer tokens = new StringTokenizer(studyFromPatient,"|");		
			while (tokens.hasMoreTokens()){
				String str = tokens.nextToken();
				if(i%2 == 0)
					patientIDs.add(str);
				else{
					studyDates.add(str.replace("-", "")); /*Format accepted by dcm4che2: yyyymmdd*/
				}
				i++;
			}
		} catch (Exception e) {
			LOG.error("The content of the file must have the following format for each line <patient ID>|yyyy-mm-dd");
			System.exit(2);
		}
		
	}
	
	protected void readIncomingFile (String ifile) throws IOException{
		/*Read a text File (.cvs) line by line */
		try {
			FileReader fr = new FileReader(ifile);
			BufferedReader bf = new BufferedReader(fr);
			ArrayList<String> content = new ArrayList<String>();
			String line;
			
			while ((line = bf.readLine())!=null) 
				if(!content.contains(line))
					content.add(line);
			
			if(!content.isEmpty())
				for(int i = 0; i < content.size(); i++)
					prepareData(content.get(i));
			else
				throw new Exception();
			
		} catch (FileNotFoundException fnfe){
			fnfe.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		} catch (Exception e){
			LOG.error("File is empty");
			System.exit(2);
		}
	}
	
	protected void writeOutcomingFile (FileWriter fw, String data){
		try {
			fw = new FileWriter(qrConfig.getOutFileName(),true);
			obw = new BufferedWriter(fw);
			obw.write(data);
		} catch (IOException e) {
				e.printStackTrace();
		}finally {
		    if (obw != null) try { obw.close(); fw.close(); } catch (IOException ignore) {}
		}
	}
	
	public void setWriter(FileWriter writer) {
		this.writer = writer;
	}

	public FileWriter getWriter() {
		return writer;
	}

	public ArrayList<String> getPatientIDs() {
		return patientIDs;
	}

	public void setPatientIDs(ArrayList<String> patientIDs) {
		this.patientIDs = patientIDs;
	}

	public ArrayList<String> getStudyDates() {
		return studyDates;
	}

	public void setStudyDates(ArrayList<String> studyDates) {
		this.studyDates = studyDates;
	}

	public void setQrConfig(DcmQrConfig qrConfig) {
		this.qrConfig = qrConfig;
	}

	public DcmQrConfig getQrConfig() {
		return qrConfig;
	}

	public void setAbortSTorage(Boolean abortSTorage) {
		this.abortSTorage = abortSTorage;
	}

	public Boolean getAbortSTorage() {
		return abortSTorage;
	}
}