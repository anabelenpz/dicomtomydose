package abpz.dicomtomydose.dcmqr;

import java.io.File;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.Tag;

import abpz.dicomtomydose.utils.OutFormatUtils;

public class DcmQrDX extends DcmQr {
	private String[] models = { "digital DIAGNOST", "SIEMENS FD-X",
			"\"Thunder Platform\"" };

	public DcmQrDX(String name) {
		super(name);
	}

	@Override
	protected Boolean fulfillModalityRequirements(BasicDicomObject fmi) {
		if (fmi.getString(Tag.ManufacturerModelName) != null)
			for (int i = 0; i < models.length; i++)
				if (fmi.getString(Tag.ManufacturerModelName).equals(models[i])) {
					if (!currentEquipment.equals(models[i]) || newQuery)
						activeHeaders = true;
					imageAnalysis = true;
				}
		return imageAnalysis;
	}

	@Override
	protected String prepareModalityValues(BasicDicomObject fmi, File file) {
		String dxValues = "";

		int radiationDoseTags[] = { Tag.ViewPosition, Tag.FieldOfViewShape,
				Tag.FieldOfViewDimensions, Tag.FieldOfViewRotation, Tag.KVP,
				Tag.XRayTubeCurrent, Tag.Exposure, Tag.ExposureTime,
				Tag.DistanceSourceToDetector, Tag.DistanceSourceToPatient,
				Tag.DistanceSourceToEntrance,
				Tag.ImageAndFluoroscopyAreaDoseProduct,
				Tag.CommentsOnRadiationDose, Tag.FilterType,
				Tag.FilterMaterial, Tag.FocalSpots, Tag.Grid };

		int doubleValueTags[] = { Tag.KVP, Tag.XRayTubeCurrent, Tag.Exposure,
				Tag.ExposureTime, Tag.DistanceSourceToDetector,
				Tag.DistanceSourceToPatient, Tag.DistanceSourceToEntrance,
				Tag.ImageAndFluoroscopyAreaDoseProduct };

		int diDiPrivateTags[] = { Tag.Sensitivity };
		int aristosPrivateTags[] = { Tag.RelativeXRayExposure };
		int definiumPrivateTags[] = { Tag.toTag("00111006") /* Image dose */,
				Tag.toTag("00111031") /* Detected Exposure Index */};

		dxValues += OutFormatUtils.formatSpecificValues(radiationDoseTags,
				doubleValueTags, fmi);

		/* Printing private EI values */
		if (fmi.getString(Tag.ManufacturerModelName).equals(models[0]))
			dxValues += OutFormatUtils.formatSpecificValues(diDiPrivateTags,
					null, fmi);

		if (fmi.getString(Tag.ManufacturerModelName).equals(models[1]))
			dxValues += OutFormatUtils.formatSpecificValues(aristosPrivateTags,
					null, fmi);

		if (fmi.getString(Tag.ManufacturerModelName).equals(models[2]))
			dxValues += OutFormatUtils.formatSpecificValues(
					definiumPrivateTags, null, fmi);

		return dxValues;
	}

	@Override
	protected String prepareModalityHeader(BasicDicomObject fmi, File file,
			Boolean active) {
		String dxHeader = "";
		if (active) {
			dxHeader += "View Position;Field of View Shape;Field of View Dimensions(mm);Field of View Rotation(degrees);"
					+ "KVp;X-Ray Tube Current (mA);Exposure(mAs);Exposure Time (ms);Distance Source to Detector;"
					+ "Distance Source to Patient;Distance Source to Entrance;Image and fluoroscopy DPA (dGy*cm²);"
					+ "Comments on Radiation Dose;Filter Type;Filter Material;Focal Spot(s) (mm);Grid;";

			/*
			 * Printing private EI headers: Exposure Index(validate by AAPM
			 * 116);
			 */
			if (fmi.getString(Tag.ManufacturerModelName).equals(models[0]))
				dxHeader += "Exposure Index (Sensibility)";

			if (fmi.getString(Tag.ManufacturerModelName).equals(models[1]))
				dxHeader += "Exposure Index (Relative X-Ray Exposure)";

			if (fmi.getString(Tag.ManufacturerModelName).equals(models[2]))
				dxHeader += "Exposure Index (Image dose); Exposure Index (Detected Exposure Index)";
		}
		return dxHeader;
	}

}