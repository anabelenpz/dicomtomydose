package abpz.dicomtomydose.utils;

import java.io.File;
import java.util.ArrayList;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

public abstract class OutFormatUtils {
	final static int identityTags [] ={
		Tag.SOPInstanceUID,
		Tag.PatientID, 
		Tag.PatientSex };
	
	final static int commonTags [] = { 
		Tag.AccessionNumber,
		Tag.StationName,
		Tag.Manufacturer, 
		Tag.ManufacturerModelName, 
		Tag.Modality,
		Tag.ImageType};
	
	/*Utilidades para dar formato al fichero de salida generado*/
	
	public static String prepareHeaderFile(BasicDicomObject fmi, Boolean active){
		String commonHeader = "";
		/*Secuencias para obtener los codigos de la SERAM*/
	
		if(active) { 
			//Common headers
			commonHeader += "\nSOP IUID;Patient ID;Sex;Age;Study Date;Study Time;Accession Number;Station Name;"
				+ "Manufacturer;Manufacturer Model;Modality;Image Type;";
			
			//Code-procedure by SERAM 
			try {
				//Code
				String code = "Code Value;Tag DICOM";
				String [] codeHeader = code.split(";");	
				commonHeader += code+";Procedure;"+codeHeader[1]+";";				
			} catch (Exception e) {
				e.printStackTrace();
			}		
		}
		return commonHeader;
	}
	
	public static String prepareCommonValues (BasicDicomObject fmi, File file){		
		String commonValues = "";
		String birthDate = null;
		String studyDate = null;
		
		/*Identity of study */
		commonValues += formatSpecificValues(identityTags, null, fmi);
		
		/*Special format for fields: Age, Study Date, Study Time */
		if (fmi.getString(Tag.PatientBirthDate) != null)
			birthDate = DateTimeUtils.formatDate(fmi.getString(Tag.PatientBirthDate));
			
		if(fmi.getString(Tag.StudyDate)!=null)
			studyDate = DateTimeUtils.formatDate(fmi.getString(Tag.StudyDate));
				
		int age = DateTimeUtils.getAges(birthDate, studyDate);
		
		commonValues += (age == 0 ? ((birthDate!=null)? birthDate+";" : ";") : age+";" )
					+ ((studyDate!= null)? studyDate : ";") +";"
					+ ((fmi.getString(Tag.StudyTime) != null) ? 
							DateTimeUtils.formatTime(fmi.getString(Tag.StudyTime)) : ";")+";";
		
		/*Rest of headerTags */
		commonValues += formatSpecificValues(commonTags, null, fmi);

		return prepareCodeProcedureValues(commonValues, fmi, file);
	}
	
	public static String prepareCodeProcedureValues (String firstvalues, BasicDicomObject fmi, File file){
		String codeProcValues = firstvalues;
		/*Sequences necessary by SERAM*/
		ArrayList<DicomObject> seqReqAttrs;
		ArrayList<DicomObject> seqProcedureCode;
		
		try{
			//Code by SERAM
			Boolean code = false;
			seqProcedureCode= D4CUtils.getDicomObjectSequence(file, Tag.ProcedureCodeSequence);
			if(seqProcedureCode != null && !seqProcedureCode.isEmpty()){
				for(int i=0;i < seqProcedureCode.size();i++)
					if(seqProcedureCode.get(i).getString(Tag.CodeValue)!= null && !code){
						codeProcValues += seqProcedureCode.get(i).getString(Tag.CodeValue)
						+";Dentro del Tag (0008,1032) >(0008,1000);";
						code = true;
					}
			}
			
			if (fmi.getString(Tag.CodeValue) != null && !code){
				codeProcValues += fmi.getString(Tag.CodeValue)+";(0008,1000);";
				code = true;
			} else if(!code)
				codeProcValues += ";;";
			
			//Procedure by SERAM
			Boolean procedure = false;
			seqReqAttrs = D4CUtils.getDicomObjectSequence(file, Tag.RequestAttributesSequence);
			if(seqReqAttrs != null && !seqReqAttrs.isEmpty()){
				for(int j=0;j < seqReqAttrs.size();j++){
					if(seqReqAttrs.get(j).getString(Tag.ScheduledProcedureStepDescription)!= null && !procedure){
						codeProcValues += seqReqAttrs.get(j).getString(Tag.ScheduledProcedureStepDescription)
								+";Dentro del Tag (0040,0275) > (0040,0007);";
						procedure = true;
					}
				}
			}
				
			if (fmi.getString(Tag.RequestedProcedureDescription) != null && !procedure){
				codeProcValues += fmi.getString(Tag.RequestedProcedureDescription)+";(0032,1060);";
				procedure = true;
						
			} else if (fmi.getString(Tag.PerformedProcedureStepDescription) != null && !procedure){
				codeProcValues += fmi.getString(Tag.PerformedProcedureStepDescription)+";(0040,0254);"; 
				procedure = true;
			
			} else if (fmi.getString(Tag.StudyDescription) != null && !procedure){
				codeProcValues += fmi.getString(Tag.StudyDescription)+";(0008,1030);";
				procedure = true;
			} else if(!procedure)		
				codeProcValues += ";;";
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return codeProcValues;
	}
	
	public static String formatSpecificValues (int [] tags, int [] doubleTags, BasicDicomObject fmi){
		String value = "";
		Boolean doubleValue = false;
		Boolean greaterVM = false;
			
		if(tags != null){
			for(int i = 0; i < tags.length; i++){
				/*For those tags whose number of elements is greater than 1 */
				if(fmi.vm(tags[i]) > 1){
					String [] values = fmi.getStrings(tags[i]);
					String fullvalue="";
					for (int j = 0; j < values.length - 1; j++) {
						fullvalue += values[j] + " \\ ";
					}
					fullvalue = fullvalue+values[values.length-1].replace(".", ",");
					value += fullvalue+";";
					greaterVM = true;
				}
				
				/*Only for those that are a double value*/
				if(doubleTags!=null){
					for(int j = 0; j < doubleTags.length; j++){
						if(tags[i] == doubleTags[j]){
							doubleValue = true;
							if (fmi.getString(tags[i]) != null && !fmi.getString(tags[i]).isEmpty()){
								value += fmi.getString(tags[i]).replace(".", ",")+";";
							} else
								value += ";";
							
						}
					}
				}			
				
				/*For all the others that are not a double value and whose number of elements is equals to 1 */
				if (fmi.getString(tags[i]) != null && !greaterVM && !doubleValue)
					value += fmi.getString(tags[i])+";";
				else if (fmi.getString(tags[i]) == null && !greaterVM && !doubleValue)
					value += ";";
				
				doubleValue = false;
				greaterVM = false;
			}
		}
		return value;
	}
}