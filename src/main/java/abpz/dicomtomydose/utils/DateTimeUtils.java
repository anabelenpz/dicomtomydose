package abpz.dicomtomydose.utils;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeUtils {
	
	public static String formatDateTime(String dateTimeFromObject) {
		/*
		 * For example, AcquisitionDateTime tag's value has this format
		 * 20120824102005 -> 24/08/2012 10:20:05
		 */		
		// Date
		String year, month, day = null;
		year = dateTimeFromObject.substring(0, 4);
		month = dateTimeFromObject.substring(4, 6);
		day = dateTimeFromObject.substring(6, 8);

		// Time
		String hour, minutes, seconds = null;
		hour = dateTimeFromObject.substring(8, 10);
		minutes = dateTimeFromObject.substring(10, 12);
		seconds = dateTimeFromObject.substring(12, 14);

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date dateTime = null;

		try {
			dateTime = sdf.parse(day + "/" + month + "/" + year + " " + hour
					+ ":" + minutes + ":" + seconds);
		} catch (ParseException e) {
			System.err.println("Cannot parse the string to java.util.Date");
			e.printStackTrace();
		}

		return sdf.format(dateTime);
	}

	public static String formatDate(String dateFromObject) {
		/*
		 * For example, StudyDate tag's value has this format 20120824 ->
		 * 24/08/2012
		 */
		String result = "unknown";
		
		if(dateFromObject != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String year, month, day = null;
	
			year = dateFromObject.substring(0, 4);
			month = dateFromObject.substring(4, 6);
			day = dateFromObject.substring(6, 8);
	
			Date date = null;
	
			try {
				date = sdf.parse(day + "/" + month + "/" + year);
			} catch (ParseException e) {
				System.err.println("Cannot parse the string to java.util.Date");
				e.printStackTrace();
			}
			result = sdf.format(date); 
		}
		
		return result;
	}

	public static String formatTime(String timeFromObject) {
		/*
		 * For example, StudyTime tag's value has this format 102005 -> 10:20:05
		 */
		String result = "unknown";
		
		if(timeFromObject != null){
			SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss");
			String hour, minutes, seconds = null;
	
			hour = timeFromObject.substring(0, 2);
			minutes = timeFromObject.substring(2, 4);
			seconds = timeFromObject.substring(4, 6);
	
			Time time = null;
	
			try {
				time = new Time(sdf.parse(hour + ":" + minutes + ":" + seconds)
						.getTime());
			} catch (ParseException e) {
				System.err.println("Cannot parse the string to java.sql.Time");
				e.printStackTrace();
			}
	
			result = sdf.format(time);
		}
		
		return result;
	}
	
	public static String getCurrentDateTime(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date dateTime = new Date();
		return sdf.format(dateTime);
	}
	
	public static String getCurrentDate(){
		return getCurrentDateTime().substring(0,10);
	}
	
	public static int getAges(String birthDate, String currentDate) {
		int age = 0;
		
		if(birthDate != null && currentDate != null){
			Calendar birthday = new GregorianCalendar();
			Calendar today = new GregorianCalendar();
			
			int factor = 0;
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date birth, current;
			
			try {
				birth = sdf.parse(birthDate);
				current = sdf.parse(currentDate);
				
				birthday.setTime(birth);
				today.setTime(current);
				
				factor = ((today.get(Calendar.MONTH) < birthday.get(Calendar.MONTH)
						|| today.get(Calendar.DATE) > birthday.get(Calendar.DATE))) ? -1 /*It's not my birth date*/ : 0;
					 
				
				age = (today.get(Calendar.YEAR) - birthday.get(Calendar.YEAR)) + factor;
			} catch (ParseException e) {
				e.printStackTrace();
			}		
		}
		
	    return age;
	}
}
