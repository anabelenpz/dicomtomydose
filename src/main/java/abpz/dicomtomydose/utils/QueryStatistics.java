package abpz.dicomtomydose.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class QueryStatistics {
	// Parameters related to DcmAnalisis statistics:
	public static Map<String, ArrayList<String>> succesful = new LinkedHashMap<String, ArrayList<String>>();
	public static Map<String, ArrayList<String>> failed = new LinkedHashMap<String, ArrayList<String>>();
	
	@SuppressWarnings("rawtypes")
	public static void extractStatistics(Map <String, ArrayList<String>> qStatistics, String fileName){
		if(qStatistics!= null && !qStatistics.isEmpty()){
			String studyIuid = null;
			String data = "";
			String header = "";
			FileWriter fw = null;
			FileReader fr = null;
			BufferedWriter bw = null;
			BufferedReader br = null;
			ArrayList<String> info;
			Iterator<?> it = qStatistics.entrySet().iterator();
			
			while(it.hasNext())
			{
				Map.Entry entry = (Map.Entry)it.next();
				studyIuid = (String) entry.getKey();
				info = qStatistics.get(studyIuid);
				
				data += studyIuid;
				
				if(info!= null && !info.isEmpty())
					for(int i = 0; i < info.size(); i++){
						data += " ; "+info.get(i);
					}
				data +="\n";
			}
			
			/*If statistics file is new. Write header */
			try {
				fr = new FileReader(fileName);
				br = new BufferedReader(fr);		
			} catch (FileNotFoundException e) {
				header ="Study Instance UID;Patient ID;Study Date;Total;Failed;Message\n";
			} finally{
				if (br != null) try { br.close(); fr.close(); } catch (IOException ignore) {}
			}
			
			try {
				fw = new FileWriter(fileName,true);
				bw = new BufferedWriter(fw);
				bw.write(header+data);
			} catch (IOException e) {
				e.printStackTrace();
			} finally{
				if (bw != null) try { bw.close(); fw.close(); } catch (IOException ignore) {}
			}
		}
	}
}
