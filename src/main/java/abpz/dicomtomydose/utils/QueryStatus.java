package abpz.dicomtomydose.utils;

import java.util.ArrayList;

public class QueryStatus {
	private String studyIuid = null;
	private String patientId = null;
	private String studyDate = null;
	private Integer moveRQId = null;
	private Integer totalInstances = null;
	private Integer remainingInstances = null;
	private ArrayList<String> info;

	public QueryStatus(String iuid, String patient, String date,
			Integer moveRQId, Integer totalInstances, Integer remainingInstances) {
		setStudyIuid(iuid);
		setPatientId(patient);
		setStudyDate(date);
		this.moveRQId = moveRQId;
		this.totalInstances = totalInstances;
		this.remainingInstances = remainingInstances;
		info = new ArrayList<String>();
	}
	
	public QueryStatus(String iuid, String patient, String date,
			Integer moveRQId, Integer totalInstances, Integer remainingInstances,String data) {
		setStudyIuid(iuid);
		setPatientId(patient);
		setStudyDate(date);
		this.moveRQId = moveRQId;
		this.totalInstances = totalInstances;
		this.remainingInstances = remainingInstances;
		info = new ArrayList<String>();
		info.add(data);
	}

	public Integer getMoveRQId() {
		return moveRQId;
	}

	public void setMoveRQId(Integer moveRQId) {
		this.moveRQId = moveRQId;
	}

	public Integer getTotalInstances() {
		return totalInstances;
	}

	public void setTotalInstances(Integer totalInstances) {
		this.totalInstances = totalInstances;
	}

	public Integer getRemainingInstances() {
		return remainingInstances;
	}

	public void setRemainingInstances(Integer remainingInstances) {
		this.remainingInstances = remainingInstances;
	}

	public void addInfo(String data) {
		info.add(data);
	}

	public void setInfo(ArrayList<String> info) {
		this.info = info;
	}

	public ArrayList<String> getInfo() {
		return info;
	}

	public void setStudyIuid(String studyIuid) {
		this.studyIuid = studyIuid;
	}

	public String getStudyIuid() {
		return studyIuid;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setStudyDate(String studyDate) {
		this.studyDate = studyDate;
	}

	public String getStudyDate() {
		return studyDate;
	}
}
