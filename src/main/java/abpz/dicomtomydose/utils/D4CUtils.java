package abpz.dicomtomydose.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.SAXWriter;

public class D4CUtils {
	
	/*Convert dcm file to xml file*/
	public static void dcmToXml(File ifile, File ofile, int [] excludes) throws TransformerConfigurationException, IOException {
		DicomInputStream dis = new DicomInputStream(ifile);
		FileOutputStream fos = new FileOutputStream(ofile);

		try {
			SAXTransformerFactory tf = (SAXTransformerFactory) TransformerFactory
            .newInstance();
			
			//Se crea un transfomerHandler nuevo, no se aplica ningun XSLT
			TransformerHandler th = tf.newTransformerHandler();	
			th.getTransformer().setOutputProperty(OutputKeys.INDENT,"yes"); 
			th.setResult(new StreamResult(fos));
			
			final SAXWriter writer = new SAXWriter(th,null);
			writer.setExclude(excludes);
			
			dis.setHandler(writer);
			dis.readDicomObject(new BasicDicomObject(), -1);
		} finally {
			if (fos != null){
				fos.close();
			}
			dis.close();
		}
	}
	
	/*Check out if a DICOM Object has a specific DICOM Object Sequence */
	public static boolean hasDicomObjectSequence(DicomObject dcmObject, int tagSQ) {
		boolean foundSQ = false;

		/* next pixel (Data set of the whole DicomObject (from group tag > 2) */
		Iterator<?> iter = dcmObject.datasetIterator();

		if(iter != null){
			while (iter.hasNext() && !foundSQ) {
				DicomElement element = (DicomElement) iter.next();
				int tag = element.tag();
	
				String tagVR = ((DicomObject) dcmObject).vrOf(tag).toString();	
				if (tagVR.equals("SQ") || element.hasDicomObjects())
					foundSQ = (element.hasItems() && tag == tagSQ);
			}
		}
		
		return foundSQ;
	}
	
	/*Get a specific DICOM Object Sequence and its items */
	public static ArrayList<DicomObject> getDicomObjectSequence(File file,
			int tagSQ) throws IOException {
		DicomInputStream dis = new DicomInputStream(file);
		DicomObject object = dis.readDicomObject();/* read metadata */
		object.remove(Tag.PixelData);

		ArrayList<DicomObject> objectSeq = new ArrayList<DicomObject>();
		boolean foundSQ = false;

		Iterator<?> iter = object.iterator();/*
											 * next pixel (Data set of the whole
											 * DicomObject)
											 */

		while (iter.hasNext() && !foundSQ) {
			DicomElement element = (DicomElement) iter.next();/* next elementData */
			int tag = element.tag();

			String tagVR = ((DicomObject) object).vrOf(tag).toString();

			/* Read data elements with VR= SQ or with DicomObjects */
			if (tagVR.equals("SQ") || element.hasDicomObjects()) {
				if (element.hasItems() && tag == tagSQ) {/* if item in metadata */
					foundSQ = true;
					for (int i = 0; i < element.countItems(); i++)
						objectSeq.add(element.getDicomObject(i));/*
																 * add items to
																 * found Dicom
																 * object
																 * Sequence
																 */

				}
			}
		}
		return objectSeq;
	}
	
	/*For DIMSE messages */
	
	/* Encoding for the Status Type Failure supported by the DIMSE services */
	public static boolean isFailure(DicomObject cmd) {
		boolean failure = false;
		if ((cmd.getInt(Tag.Status) >= 0xA000)
				&& (cmd.getInt(Tag.Status) <= 0xAFFF)){
			failure = true;
			System.out.println("Failure.");
		}
		else if ((cmd.getInt(Tag.Status) >= 0xC000)
				&& (cmd.getInt(Tag.Status) <= 0xCFFF)){
			failure = true;
			System.out.println("Failure.");
		}
		return failure;
	}
	
	public static boolean isWarning (DicomObject cmd) {
		/* Encoding for the Status Type Warning supported by the DIMSE services */
		return ((cmd.getInt(Tag.Status) == 0x0001) || cmd.getInt(Tag.Status) == 0xB000);
	}
	
	public static boolean isSuccess (DicomObject cmd) {
		/* Encoding for the Status Type Success supported by the DIMSE services */
		return ((cmd.getInt(Tag.Status) == 0x0000));
	}
}


